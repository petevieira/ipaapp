package com.vieira.pete.ipaapp

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
//import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.VideoView
import kotlinx.android.synthetic.main.activity_symbol.*
import kotlinx.android.synthetic.main.activity_symbol.view.*

/*
There was a problem with the video tracking because the videos didn't have enough keyframes,
so you can use this command with ffmpeg to add keyframes every 2 frames of the video
ffmpeg -i "~/Downloads/play_video_test.mp4" -c:v libx264 -preset superfast -x264opts keyint=2 -acodec copy -f mp4 "~/Downloads/play_video_test_key_frame.mp4"

info found at https://www.dev2qa.com/how-to-make-android-videoview-seekto-method-consistent/
 */

// Time in milliseconds below which constitutes a tap, instead of a long touch or touch and drag
const val MAX_SINGLE_TOUCH_TIME_MS = 200
// Time in milliseconds before the end of the video after which a play will first rewind the video
// because playing the last bit of the video is pointless
const val REWIND_AFTER_PAUSED_MS_BEFORE_END = 50

/**
 * Activity that handles a single symbol's page. This activity is used for all the symbols.
 * This class extends the Base Activity class, AppCompatActivity
 */
class SymbolActivity : AppCompatActivity() {

    private var firstPlay: Boolean = true
    private var videoDragButtonDownTimestamp: Long = 0
    private var videoTouchButtonDownTimestamp: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_symbol)

        val message = intent.getIntExtra(SYMBOL_MSG, 0)
        val ipa: IpaSymbol = symbolsMap.get(message)!!
        setPageContent(ipa)
        supportActionBar?.title = getString(ipa.symbol) + "  " + getString(ipa.symbolName)
    }

    private fun setPageContent(ipa: IpaSymbol) {
//        this.pageTitle.text = getString(content.symbol)
//        this.symbolName.text = getString(content.symbol_name)
        this.phoneticSymbol.text = String.format("[%s]", getString(ipa.symbol))
        this.phoneticDescription.text = getString(ipa.phoneticDescription)
        this.phoneticExplanation.text = getString(ipa.phoneticExplanation)
        this.setUpVideo(videoMri, ipa.videoMriSuffix, ipa.videoMriPreviewTimeMs)
    }

    private fun setUpVideo(video: VideoView, videoFileSuffix: Int, videoPreviewTimeMs: Int) {
        // If we don't have a video suffix, then there is no video for this symbol, so
        // hide the video
        if (getString(videoFileSuffix).isEmpty()) {
            video.visibility = View.INVISIBLE
            return
        }

        // Create and set path to video based on what button was pressed on the previous screen
        val videoPath = "android.resource://$packageName/" + videoFileSuffix
        video.setVideoURI(Uri.parse(videoPath))

        // What to do when the video is ready to be used
        video.setOnPreparedListener {
            video.seekTo(videoPreviewTimeMs)
//            VideoProgressThread().execute()
        }

        // What to do on touches
        videoTouchDragView.setOnTouchListener { view: View, motionEvent: MotionEvent ->
            // Play and Pause the video
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                if ((motionEvent.eventTime - videoDragButtonDownTimestamp) < MAX_SINGLE_TOUCH_TIME_MS) {
                    if (video.isPlaying) {
                        video.pause()
                    } else {
                        if (video.currentPosition >= video.duration - REWIND_AFTER_PAUSED_MS_BEFORE_END) {
                            video.seekTo(0)
                        }
                        if (firstPlay) {
                            video.seekTo(0)
                            firstPlay = false
                        }
                        video.start()
                    }
                }
            } else if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                // Get Timestamp of touch
                videoDragButtonDownTimestamp = motionEvent.eventTime
            } else if (motionEvent.action == MotionEvent.ACTION_MOVE) {
                if ((motionEvent.eventTime - videoDragButtonDownTimestamp) > MAX_SINGLE_TOUCH_TIME_MS) {
                    // Tracking video with touch and drag motion
                    val newTimeMs = (motionEvent.x / view.width * video.duration).toInt()
                    if (newTimeMs >= 0 && newTimeMs <= video.duration) {
                        video.seekTo(newTimeMs)
                    }
                }
            }
            true
        }

        videoSingleTouchView.setOnTouchListener { _, motionEvent: MotionEvent ->
            // Play and Pause the video
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                if ((motionEvent.eventTime - videoTouchButtonDownTimestamp) < MAX_SINGLE_TOUCH_TIME_MS) {
                    if (video.isPlaying) {
                        video.pause()
                    } else {
                        if (video.currentPosition >= video.duration - REWIND_AFTER_PAUSED_MS_BEFORE_END) {
                            video.seekTo(0)
                        }
                        if (firstPlay) {
                            video.seekTo(0)
                            firstPlay = false
                        }
                        video.start()
                    }
                }
            } else if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                // Get Timestamp of touch
                videoTouchButtonDownTimestamp = motionEvent.eventTime
            }
            true
        }

        // What to do when the video completes
        video.setOnCompletionListener {
            video.seekTo(0)
        }
    }

//    inner class VideoProgressThread : AsyncTask<String, Int, String>() {
//        override fun doInBackground(vararg p0: String?): String {
//            do {
//                publishProgress(videoMri.currentPosition)
//            } while (videoMri.isShown)
//            return ""
//        }
//
//        override fun onProgressUpdate(vararg values: Int?) {
//            super.onProgressUpdate(*values)
//            videoMriProgress.text = String.format("%d ms", values[0])
//        }
//    }

    private data class Content (
            var symbol: Int,
            var symbol_name: Int,
            val videoMriSuffix: Int,
            var videoMriPreviewTimeMs: Int,
            var videoAniSuffix: Int?,
            var phoneticDescription: Int,
            var phoneticExplanation: String
    )
}
