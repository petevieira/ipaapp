package com.vieira.pete.ipaapp

import android.content.Intent
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_main)

        // Set up buttons
        this.setClickListeners()

        val typeface = Typeface.createFromAsset(this.assets, "fonts/${this.resources.getString(R.string.font_main_buttons)}")
        buttonConsonantsPulmonic.typeface = typeface
        buttonConsonantsNonPulmonic.typeface = typeface
        buttonVowels.typeface = typeface
        buttonDiacritics.typeface = typeface
        buttonOtherSymbols.typeface = typeface
        buttonSuprasegmentals.typeface = typeface
        buttonTones.typeface = typeface
        buttonIpaChart.typeface = typeface
        buttonLanguageIpaCharts.typeface = typeface

//        val displaySize: Point = Point(image.layoutParams.width, image.layoutParams.height)
//        val display = windowManager.defaultDisplay.getSize(displaySize)
//        val drawable: Drawable = resources.getDrawable(R.drawable.ipa_chart_2018)
//        val aspectRatio = drawable.intrinsicWidth.toFloat() / drawable.intrinsicHeight.toFloat()
//        image.layoutParams.width = displaySize.x
//        image.layoutParams.height = (displaySize.x / aspectRatio).toInt()

//        val spannable = SpannableString(supportActionBar?.title)
//        spannable.setSpan(typeface, 0, spannable.length, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE)
//        supportActionBar?.title = spannable
    }

    private fun setClickListeners() {
        buttonConsonantsPulmonic.setOnClickListener {
            openConsonants()
        }

        buttonVowels.setOnClickListener {
            openVowels()
        }

        buttonConsonantsNonPulmonic.setOnClickListener {
            openConsonantsNonPulmonic()
        }

        buttonOtherSymbols.setOnClickListener {
            openOtherSymbols()
        }

        buttonDiacritics.setOnClickListener {
            openDiacritics()
        }

        buttonSuprasegmentals.setOnClickListener {
            openSuprasegmentals()
        }

        buttonTones.setOnClickListener {
            openTones()
        }

        buttonIpaChart.setOnClickListener {
            openIpaChart()
        }

        buttonLanguageIpaCharts.setOnClickListener {
            openLanguageKeys()
        }
    }

    private fun openConsonants() {
        // Create intent to start consonants activity
        val intent = Intent(this, ConsonantsActivity::class.java).apply {
            // Not passing any arguments to the new activity in the intent
        }
        // Start the consonants activity
        startActivity(intent)
    }

    private fun openVowels() {
        val intent = Intent(this, VowelsActivity::class.java).apply {
        }
        // Start the vowels activity
        startActivity(intent)
    }

    private fun openConsonantsNonPulmonic() {
        val intent = Intent(this, ConsonantsNonPulmonicActivity::class.java).apply {
        }
        // Start the consonant non-pulmonic activity
        startActivity(intent)
    }

    private fun openOtherSymbols() {
        val intent = Intent(this, OtherSymbolsActivity::class.java).apply {
        }
        // Start the other symbols activity
        startActivity(intent)
    }

    private fun openDiacritics() {
        val intent = Intent(this, DiacriticsActivity::class.java).apply {
        }
        // Start the diacritics activity
        startActivity(intent)
    }

    private fun openSuprasegmentals() {
        val intent = Intent(this, SuprasegmentalsActivity::class.java).apply {
        }
        // Start the suprasegmentals activity
        startActivity(intent)
    }

    private fun openTones() {
        val intent = Intent(this, TonesActivity::class.java).apply {
        }
        // Start the tones activity
        startActivity(intent)
    }


    private fun openIpaChart() {
        val intent = Intent(this, IpaChartActivity::class.java).apply {
        }
        startActivity(intent)
    }

    private fun openLanguageKeys() {
        val intent = Intent(this, LanguageKeysActivity::class.java).apply {
        }
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the ellipsis menu. This adds items to the action bar if it is present
        this.menuInflater.inflate(R.menu.menu_ellipsis, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Handle action bar (ellipsis menu) item clicks here. The action bar will automatically
        // handle clicks on the Home/Up button, so long as you specify a parent activity in
        // AndroidManifest.xml
        return when (item?.itemId) {
            R.id.action_settings -> true
            else -> return super.onOptionsItemSelected(item)
        }
    }


}
