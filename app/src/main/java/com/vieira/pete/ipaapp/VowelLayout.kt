package com.vieira.pete.ipaapp

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_vowels.view.*

/**
 * Layout that extends the ConstraintLayout in order to draw the lines on the background.
 * This was chosen instead of using a background image, etc. because that is jarring and can't
 * maintain color theme changes.
 */
class VowelLayout @JvmOverloads constructor(context: Context?, attrs: AttributeSet? = null,
                                            defStyleAttr: Int = 0) : ConstraintLayout(context,attrs, defStyleAttr) {
    // Helper variables
    private var v11dotX = 0f
    private var v11dotY = 0f
    private var v12dotX = 0f
    private var v12dotY = 0f
    private var v13dotX = 0f
    private var v13dotY = 0f

    private var v21dotX = 0f
    private var v21dotY = 0f
    private var v22dotX = 0f
    private var v22dotY = 0f
    private var v23dotX = 0f
    private var v23dotY = 0f

    private var v31dotX = 0f
    private var v31dotY = 0f
    private var v32dotX = 0f
    private var v32dotY = 0f
    private var v33dotX = 0f
    private var v33dotY = 0f

    private var v41dotX = 0f
    private var v41dotY = 0f
    private var v42dotX = 0f
    private var v42dotY = 0f
    private var v43dotX = 0f
    private var v43dotY = 0f

    // Paint for lines
    private val linePaint = Paint(ANTI_ALIAS_FLAG).apply {
        var typedValue = TypedValue()
        context!!.theme.resolveAttribute(R.attr.textColorPrimary, typedValue, true)
        color = typedValue.data

        strokeWidth = 3f
    }

    public fun setStrokeWidth(width: Float) {
        this.linePaint.strokeWidth = width
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        val margin = resources.getDimension(R.dimen.vowel_button_inner_margin)

        // Corners
        v11dotX = v11dot.x + v11dot.width/2
        v11dotY = v11dot.y + v11dot.height/2
        v41dotX = v43dot.x * 0.5f
        v41dotY = v41dot.y + v41dot.height/2
        v13dotX = (buttonLowerCaseU as View).x - margin
        v13dotY = v13dot.y + v13dot.height/2
        v43dotX = v13dotX - margin
        v43dotY = v43dot.y + v43dot.height/2

        v12dotX = (v11dotX + v13dotX)/2
        v12dotY = v11dotY
        v42dotX = (v41dotX + v43dotX)/2
        v42dotY = v41dotY

        v21dotX = getViewCenterX(v11dotX, v41dotX, v11dotY, v41dotY, v21dot)
        v21dotY = (v41dotY - v11dotY)/3 + v11dotY
        v22dotX = getViewCenterX(v12dotX, v42dotX, v12dotY, v42dotY, v22dot)
        v22dotY = v21dotY
        v23dotX = v13dotX - margin/4
        v23dotY = v21dotY

        v31dotX = getViewCenterX(v11dotX, v41dotX, v11dotY, v41dotY, v31dot)
        v31dotY = (v41dotY - v11dotY)*2/3 + v11dotY
        v32dotX = getViewCenterX(v12dotX, v42dotX, v12dotY, v42dotY, v32dot)
        v32dotY = v31dotY
        v33dotX = v13dotX - margin/2
        v33dotY = v31dotY

        canvas.apply {
            // Draw the vertical-ish lines on the canvas
            drawLines(this!!)

            // Move the dots, in x, onto their appropriate lines
            moveDotsOntoLines()

            // Move buttons to be centered around dots
            moveButtonsToCenterAroundDots()

            // Move headers to be centered at dots
            moveHeaderstoCenterAtDots()
        }
    }

    private fun drawLines(canvas: Canvas) {
        /* VERTICAL LINES */
        // Draw first angled line
        canvas.drawLine(v11dotX, v11dotY, v41dotX, v41dotY, linePaint)
        // Draw second angled line
        canvas.drawLine(v12dotX, v12dotY, v42dotX, v42dotY, linePaint)
        // Draw vertical line
        canvas.drawLine(v13dotX, v13dotY, v43dotX, v43dotY, linePaint)

        /* HORIZONTAL LINES */
        // Draw top horizontal line
        canvas.drawLine(v11dotX, v11dotY, v13dotX, v13dotY, linePaint)
        // Draw 2nd horizontal line
        canvas.drawLine(v21dotX, v21dotY, v23dotX, v23dotY, linePaint)
        // Draw 3rd horizontal line
        canvas.drawLine(v31dotX, v31dotY, v33dotX, v33dotY, linePaint)
        // Draw bottom horizontal line
        canvas.drawLine(v41dotX, v41dotY, v43dotX, v43dotY, linePaint)
    }

    private fun moveDotsOntoLines() {
        v11dot.x = v11dotX - v11dot.width/2
        v11dot.y = v11dotY - v11dot.height/2
        v12dot.x = v12dotX - v12dot.width/2
        v12dot.y = v12dotY - v12dot.height/2
        v21dot.x = v21dotX - v21dot.width/2
        v31dot.x = v31dotX - v31dot.width/2
        v41dot.x = v41dotX - v41dot.width/2
        v22dot.x = v22dotX - v22dot.width/2
        v32dot.x = v32dotX - v32dot.width/2
        v13dot.x = v13dotX - v13dot.width/2
        v23dot.x = v23dotX - v23dot.width/2
        v23dot.y = v23dotY - v23dot.height/2
        v33dot.x = v33dotX - v33dot.width/2
        v43dot.x = v43dotX - v43dot.width/2
    }

    private fun getViewCenterX(x1: Float, x2: Float, y1: Float, y2: Float, view: View) : Float {
        val slopeM = (y2 - y1) / (x2 - x1)
        val yCrossingB = y1 - slopeM * x1
        return ((view.y + view.height/2) - yCrossingB) / slopeM
    }

    private fun moveButtonsToCenterAroundDots() {
        val margin = resources.getDimension(R.dimen.vowel_button_inner_margin)
        // Fix margins for buttons in row 1
        buttonLowerCaseI.x = v11dotX - buttonLowerCaseI.width - margin
        buttonLowerCaseY.x = v11dotX + margin

        buttonBarredI.x = v12dotX - buttonBarredI.width - margin
        buttonBarredU.x = v12dotX + margin

        buttonTurnedM.x = v13dotX - buttonTurnedM.width - margin
        buttonLowerCaseU.x = v13dotX + margin

        // Fix margins for buttons in row 2
        buttonLowerCaseE.x = v21dotX - buttonLowerCaseE.width - margin
        buttonSlashedO.x = v21dotX + margin

        buttonReversedE.x = v22dotX - buttonReversedE.width - margin
        buttonBarredO.x = v22dotX + margin

        buttonRamsHorns.x = v23dotX - buttonRamsHorns.width - margin
        buttonLowerCaseO.x = v23dotX + margin

        // Fix margins for buttons in row 3
        buttonEpsilon.x = v31dotX - buttonEpsilon.width - margin
        buttonLowerCaseOELigature.x = v31dotX + margin

        buttonReversedEpsilon.x = v32dotX - buttonReversedEpsilon.width - margin
        buttonClosedReversedEpsilon.x = v32dotX + margin

        buttonTurnedV.x = v33dotX - buttonTurnedV.width - margin
        buttonOpenO.x = v33dotX + margin

        // Fix margins for buttons in row 4
        buttonLowerCaseA.x = v41dotX - buttonLowerCaseA.width - margin
        buttonSmallCapitalOELigature.x = v41dotX + margin
        buttonScriptA.x = v43dotX - buttonScriptA.width - margin
        buttonTurnedScriptA.x = v43dotX + margin

        // Fix margins for in between buttons
        buttonAsh.x = getViewCenterX(v11dotX, v41dotX, v11dotY, v41dotY, buttonAsh) - buttonAsh.width - margin
        buttonSchwa.x = getViewCenterX(v12dotX, v42dotX, v12dotY, v42dotY, buttonSchwa) - buttonSchwa.width/2
        buttonTurnedA.x = getViewCenterX(v12dotX, v42dotX, v12dotY, v42dotY, buttonTurnedA) - buttonTurnedA.width/2
        buttonUpsilon.x = buttonTurnedM.x - buttonUpsilon.width
    }

    private fun moveHeaderstoCenterAtDots() {
        textFront.x = v11dotX - textFront.width/2
        textCenter.x = v12dotX - textCenter.width/2
        textBack.x = v13dotX - textBack.width/2
    }
}