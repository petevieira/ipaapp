package com.vieira.pete.ipaapp

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_ipa_videos.*
import com.vieira.pete.ipaapp.IpaGenericActivity.*

class IpaVideosFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ipa_videos, container, false)
    }

    fun setUpVideo(ipa: IpaSymbol, lastIpaSymbol: IpaSymbol?, currentVideoType: TabEnum, startVideo: Boolean = true) {
        // Which videos exists for this IPA symbol?
        val haveAnimationVideo = resources.getString(ipa.videoAnimatedSuffix).isNotEmpty()
        val haveMriVideo = resources.getString(ipa.videoMriSuffix).isNotEmpty()
        if (currentVideoType == TabEnum.TAB_ANIMATION && haveAnimationVideo) {
            // Set up the animation video if it exists, it's the current tab and the IPA symbol changed.
            if (lastIpaSymbol != ipa) {
                setUpVideoView(context,
                        videoAnimated,
                        videoTouchDragView,
                        videoSingleTouchView,
                        ipa.videoAnimatedSuffix,
                        ipa.videoAnimatedPreviewTimeMs)
                // Make visible in case it was made invisible earlier due to a missing video
                videoAnimated.visibility = View.VISIBLE
                videoMri.visibility = View.INVISIBLE
            }
            // Play the video from the beginning
            if (startVideo) {
                startVideoFromZero(context, videoAnimated, ipa.videoAnimatedSuffix)
            } else {
                videoAnimated.seekTo(50)
            }
        } else if (haveMriVideo) {
//        } else if (currentVideoType == TabEnum.TAB_MRI && haveMriVideo) {
            // Set up the MRI video if it exists, it's the current tab and the IPA symbol changed.
            if (lastIpaSymbol != ipa) {
                setUpVideoView(context,
                        videoMri,
                        videoTouchDragView,
                        videoSingleTouchView,
                        ipa.videoMriSuffix,
                        ipa.videoMriPreviewTimeMs)
                // Make visible in case it was made invisible earlier due to a missing video
                videoMri.visibility = View.VISIBLE
                videoAnimated.visibility = View.INVISIBLE
            }
            // Play the video from the beginning
            if (startVideo) {
                startVideoFromZero(context, videoMri, ipa.videoMriSuffix)
            } else {
                videoMri.seekTo(50)
            }
        } else {
            // Otherwise if no video exists, make video invisible
            videoAnimated.visibility = View.INVISIBLE
            videoMri.visibility = View.INVISIBLE
        }
    }
}
