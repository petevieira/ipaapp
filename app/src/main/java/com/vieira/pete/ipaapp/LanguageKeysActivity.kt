package com.vieira.pete.ipaapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.WebViewClient
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_language_keys.*

class LanguageKeysActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language_keys)

        setUpSpinner()



    }

    private fun setUpSpinner() {
        spinner.prompt = "Select Language"
        ArrayAdapter.createFromResource(this, R.array.languages, R.layout.support_simple_spinner_dropdown_item).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = this
    }

    private fun setUpWebView() {
        webView.settings.javaScriptEnabled = true
        webView.settings.builtInZoomControls = true
        val wvc = WebViewClient()
        webView.webViewClient = wvc
        webView.canGoBack()
        webView.canGoForward()
//        webView.loadUrl("android.resource://${this.packageName}/" + R.raw.berber_languages_Wikipedia)
        webView.loadUrl("https://en.wikipedia.org/wiki/Help:IPA/Hindi_and_Urdu")
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            webView.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        p0?.getItemAtPosition(p2)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }
}
