package com.vieira.pete.ipaapp

import android.os.Bundle

class TonesActivity : IpaGenericActivity() {

   init {
       val data = Tones()
       // Create sections data inefficiently
       val temp: MutableList<SectionData> = mutableListOf<SectionData>()
       for (i in 0..data.sections.size-1) {
           temp.add(SectionData(data.sections[i], IntRange(0, data.tones[i].size-1).map {
               RowData(data.tones_places_of_articulation[i][it], data.tones[i][it])}))
       }
       sections = temp

       ipaAdapter = IpaGenericRecyclerViewAdapter(
               this.sections, this, R.layout.section_header_no_columns, 2)
       ipaAdapter.setDisplayMiddleText(R.string.tones_middle_text)

   }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_ipa_generic)
        setUpRecyclerView()
        setUpFragmentViewSizes()
        setUpTabCallbacks()

        setUpIpaViews(symbolsMap.get(R.string.example_level_extra_high))
    }
}
