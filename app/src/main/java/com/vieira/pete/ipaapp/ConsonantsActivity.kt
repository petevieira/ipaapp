package com.vieira.pete.ipaapp

import android.os.Bundle

class ConsonantsActivity : IpaGenericActivity() {

    init {
        val data = ConsonantsPulmonicData()
        sections = IntRange(0, data.sections.size - 1).map { it ->
            SectionData(data.sections[it], (data.rows.map { row ->
                IntRange(0, data.row_labels.size - 1).map { k ->
                    RowData(data.row_labels[k], row[k])
                }
            })[it]
            )
        }

        ipaAdapter = IpaGenericRecyclerViewAdapter(
                this.sections,this, R.layout.section_header, 2)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_ipa_generic)
        setUpRecyclerView()
        setUpFragmentViewSizes()
        setUpTabCallbacks()

        setUpIpaViews(symbolsMap.get(R.string.symbol_lower_case_p))
    }
}
