package com.vieira.pete.ipaapp

import android.graphics.Point
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_vowels.*
import kotlinx.android.synthetic.main.fragment_ipa_symbol_info.*

class VowelsActivity : IpaGenericActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_vowels)
        this.setUpFragmentViewSizes()
        super.setUpTabCallbacks()
        this.setUpVowelButtonCallbacks()

        setUpIpaViews(symbolsMap.get(R.string.symbol_lower_case_i))
    }

    override fun setUpFragmentViewSizes() {
        // Set the column sizes for the symbol information text below the video
        val displaySize = Point(vowelsTopLayout.layoutParams.width,
                vowelsTopLayout.layoutParams.height)
        windowManager.defaultDisplay.getSize(displaySize)
        symbolSymbol.layoutParams.width = displaySize.x * 1 / 9
        symbolName.layoutParams.width = displaySize.x * 3 / 9
        symbolDescription.layoutParams.width = displaySize.x * 5 / 9
        headerName.layoutParams.width = displaySize.x * 3 / 9
        headerDescription.layoutParams.width = displaySize.x * 5 / 9
    }

    private fun setUpVowelButtonCallbacks() {
        // Set up button specific listeners
        buttonLowerCaseI.setOnClickListener { createOnClickListener(R.string.symbol_lower_case_i) }
        buttonLowerCaseY.setOnClickListener { createOnClickListener(R.string.symbol_lower_case_y) }
        buttonBarredI.setOnClickListener { createOnClickListener(R.string.symbol_barred_i) }
        buttonBarredU.setOnClickListener { createOnClickListener(R.string.symbol_barred_u) }
        buttonTurnedM.setOnClickListener { createOnClickListener(R.string.symbol_turned_m) }
        buttonLowerCaseU.setOnClickListener { createOnClickListener(R.string.symbol_lower_case_u) }

        buttonSmallCapitalI.setOnClickListener { createOnClickListener(R.string.symbol_small_capital_i) }
        buttonSmallCapitalY.setOnClickListener { createOnClickListener(R.string.symbol_small_capital_y) }
        buttonUpsilon.setOnClickListener { createOnClickListener(R.string.symbol_upsilon) }

        buttonLowerCaseE.setOnClickListener { createOnClickListener(R.string.symbol_lower_case_e) }
        buttonSlashedO.setOnClickListener { createOnClickListener(R.string.symbol_slashed_o) }
        buttonReversedE.setOnClickListener { createOnClickListener(R.string.symbol_reversed_e) }
        buttonBarredO.setOnClickListener { createOnClickListener(R.string.symbol_barred_o) }
        buttonRamsHorns.setOnClickListener { createOnClickListener(R.string.symbol_rams_horns) }
        buttonLowerCaseO.setOnClickListener { createOnClickListener(R.string.symbol_lower_case_o) }

        buttonSchwa.setOnClickListener { createOnClickListener(R.string.symbol_schwa) }

        buttonEpsilon.setOnClickListener { createOnClickListener(R.string.symbol_epsilon) }
        buttonLowerCaseOELigature.setOnClickListener { createOnClickListener(R.string.symbol_lower_case_o_e_ligature) }
        buttonReversedEpsilon.setOnClickListener { createOnClickListener(R.string.symbol_reversed_epsilon) }
        buttonClosedReversedEpsilon.setOnClickListener { createOnClickListener(R.string.symbol_closed_reversed_epsilon) }
        buttonTurnedV.setOnClickListener { createOnClickListener(R.string.symbol_turned_v) }
        buttonOpenO.setOnClickListener { createOnClickListener(R.string.symbol_open_o) }

        buttonAsh.setOnClickListener { createOnClickListener(R.string.symbol_ash) }
        buttonTurnedA.setOnClickListener { createOnClickListener(R.string.symbol_turned_a) }

        buttonLowerCaseA.setOnClickListener { createOnClickListener(R.string.symbol_lower_case_a) }
        buttonSmallCapitalOELigature.setOnClickListener { createOnClickListener(R.string.symbol_small_capital_o_e_ligature) }
        buttonScriptA.setOnClickListener { createOnClickListener(R.string.symbol_script_a) }
        buttonTurnedScriptA.setOnClickListener { createOnClickListener(R.string.symbol_turned_script_a) }
    }

    private fun createOnClickListener(symbol: Int) {
        val ipa: IpaSymbol? = symbolsMap.get(symbol)
        if (ipa == null) {
            Log.d("Symbol is null:", resources.getString(symbol))
            return
        } else {
            setUpIpaViews(ipa)
        }
    }
}
