package com.vieira.pete.ipaapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

/**
 * Activity that serves as an introduction to the app.
 * Topics include:
 *      - Purpose
 *      - Background
 *      - Audience
 *      - Features
 *      - Notes
 */

class IntroductionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_introduction)
    }
}
