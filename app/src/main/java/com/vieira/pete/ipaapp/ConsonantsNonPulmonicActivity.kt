package com.vieira.pete.ipaapp

import android.os.Bundle


class ConsonantsNonPulmonicActivity : IpaGenericActivity() {
    init {
        // Create sections data inefficiently
        val data = ConsonantsNonPulmonicData()
        val temp: MutableList<SectionData> = mutableListOf<SectionData>()
        for (i in 0 until data.sections.size) {
            temp.add(SectionData(data.sections[i], IntRange(0, data.rows[i].size - 1).map {
                RowData(data.row_labels[i][it], listOf(data.rows[i][it]))
            }))
        }
        sections = temp

        ipaAdapter = IpaGenericRecyclerViewAdapter(
                this.sections,this, R.layout.section_header_no_columns, 1)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_ipa_generic)
        setUpRecyclerView()
        setUpFragmentViewSizes()
        setUpTabCallbacks()

        setUpIpaViews(symbolsMap.get(R.string.symbol_bulls_eye))
    }
}
