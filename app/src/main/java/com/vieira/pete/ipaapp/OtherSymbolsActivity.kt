package com.vieira.pete.ipaapp

import android.os.Bundle

class OtherSymbolsActivity : IpaGenericActivity() {

    init {
        val data = OtherSymbols()
        sections = IntRange(0, data.sections.size-1).map {
            SectionData(data.sections[it], IntRange(0, data.other_symbols.size-1).map { row ->
                RowData(data.places_of_articulation[row], listOf(data.other_symbols[row]))
            })
        }

        ipaAdapter = IpaGenericRecyclerViewAdapter(
                this.sections, this, R.layout.section_header_no_columns, 1)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_ipa_generic)
        setUpRecyclerView()
        setUpFragmentViewSizes()
        setUpTabCallbacks()

        setUpIpaViews(symbolsMap.get(R.string.symbol_turned_w))
    }
}
