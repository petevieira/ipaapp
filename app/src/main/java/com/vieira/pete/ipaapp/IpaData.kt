package com.vieira.pete.ipaapp

import android.os.Parcel
import android.os.Parcelable

data class RowData(
    val rowTitle: Int,
    val buttons: List<Int>
)

data class SectionData(
    val sectionTitle: Int,
    val rows: List<RowData>
)

data class IpaSymbol(
    var symbol: Int,
    var symbolName: Int,
    var phoneticDescription: Int,
    var phoneticExplanation: Int,
    var videoMriSuffix: Int,
    var videoMriPreviewTimeMs: Int,
    var videoAnimatedSuffix: Int,
    var videoAnimatedPreviewTimeMs: Int
)

val symbolsMap: HashMap<Int, IpaSymbol> = hashMapOf(
        R.string.symbol_lower_case_a to IpaSymbol(R.string.symbol_lower_case_a, R.string.lower_case_a, R.string.descr_lower_case_a, R.string.explanation_lower_case_a, R.raw.front_open_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_turned_a to IpaSymbol(R.string.symbol_turned_a, R.string.turned_a, R.string.descr_turned_a, R.string.explanation_turned_a, R.raw.central_nearopen_mri, 500, R.string.empty_string, 500),
        R.string.symbol_script_a to IpaSymbol(R.string.symbol_script_a, R.string.script_a, R.string.descr_script_a, R.string.explanation_script_a, R.raw.back_open_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_turned_script_a to IpaSymbol(R.string.symbol_turned_script_a, R.string.turned_script_a, R.string.descr_turned_script_a, R.string.explanation_turned_script_a, R.raw.back_open_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_ash to IpaSymbol(R.string.symbol_ash, R.string.ash, R.string.descr_ash, R.string.explanation_ash, R.raw.front_nearopen_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_b to IpaSymbol(R.string.symbol_lower_case_b, R.string.lower_case_b, R.string.descr_lower_case_b, R.string.explanation_lower_case_b, R.raw.bilabial_plosive_voiced_mri, 500, R.raw.bilabial_plosive_voiced_animated, 500),
        R.string.symbol_hooktop_b to IpaSymbol(R.string.symbol_hooktop_b, R.string.hooktop_b, R.string.descr_hooktop_b, R.string.explanation_hooktop_b, R.raw.bilabial_voiced_implosive_mri, 500, R.string.empty_string, 500),
        R.string.symbol_small_capital_b to IpaSymbol(R.string.symbol_small_capital_b, R.string.small_capital_b, R.string.descr_small_capital_b, R.string.explanation_small_capital_b, R.raw.bilabial_trill_mri, 500, R.raw.bilabial_trill_animated, 500),
        R.string.symbol_beta to IpaSymbol(R.string.symbol_beta, R.string.beta, R.string.descr_beta, R.string.explanation_beta, R.raw.bilabial_fricative_voiced_mri, 500, R.raw.bilabial_fricative_voiced_animated, 500),
        R.string.symbol_lower_case_c to IpaSymbol(R.string.symbol_lower_case_c, R.string.lower_case_c, R.string.descr_lower_case_c, R.string.explanation_lower_case_c, R.raw.palatal_plosive_voiceless_mri, 500, R.raw.palatal_plosive_voiceless_animated, 500),
        R.string.symbol_hooktop_c to IpaSymbol(R.string.symbol_hooktop_c, R.string.hooktop_c, R.string.descr_hooktop_c, R.string.explanation_hooktop_c, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_c_wedge to IpaSymbol(R.string.symbol_c_wedge, R.string.c_wedge, R.string.descr_c_wedge, R.string.explanation_c_wedge, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_c_cedilla to IpaSymbol(R.string.symbol_c_cedilla, R.string.c_cedilla, R.string.descr_c_cedilla, R.string.explanation_c_cedilla, R.raw.palatal_fricative_voiceless_mri, 500, R.raw.palatal_fricative_voiceless_animated, 500),
        R.string.symbol_curly_tail_c to IpaSymbol(R.string.symbol_curly_tail_c, R.string.curly_tail_c, R.string.descr_curly_tail_c, R.string.explanation_curly_tail_c, R.raw.alveolopalatal_fricative_voiceless_mri, 500, R.string.empty_string, 500),
        R.string.symbol_stretched_c to IpaSymbol(R.string.symbol_stretched_c, R.string.stretched_c, R.string.descr_stretched_c, R.string.explanation_stretched_c, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_d to IpaSymbol(R.string.symbol_lower_case_d, R.string.lower_case_d, R.string.descr_lower_case_d, R.string.explanation_lower_case_d, R.raw.alveolar_plosive_voiced_mri, 500, R.raw.alveolar_plosive_voiced_animated, 500),
        R.string.symbol_hooktop_d to IpaSymbol(R.string.symbol_hooktop_d, R.string.hooktop_d, R.string.descr_hooktop_d, R.string.explanation_hooktop_d, R.raw.dental_alveolar_voiced_implosive_mri, 500, R.string.empty_string, 500),
        R.string.symbol_right_tail_d to IpaSymbol(R.string.symbol_right_tail_d, R.string.right_tail_d, R.string.descr_right_tail_d, R.string.explanation_right_tail_d, R.raw.retroflex_plosive_voiced_mri, 500, R.raw.retroflex_plosive_voiced_animated, 500),
        R.string.symbol_d_z_ligature to IpaSymbol(R.string.symbol_d_z_ligature, R.string.d_z_ligature, R.string.descr_d_z_ligature, R.string.explanation_d_z_ligature, R.raw.alveolar_affricate_voiced_mri, 500, R.string.empty_string, 500),
        R.string.symbol_d_ezh_ligature to IpaSymbol(R.string.symbol_d_ezh_ligature, R.string.d_ezh_ligature, R.string.descr_d_ezh_ligature, R.string.explanation_d_ezh_ligature, R.raw.postalveolar_affricate_voiced_mri, 500, R.string.empty_string, 500),
        R.string.symbol_d_curly_tail_z_ligature to IpaSymbol(R.string.symbol_d_curly_tail_z_ligature, R.string.d_curly_tail_z_ligature, R.string.descr_d_curly_tail_z_ligature, R.string.explanation_d_curly_tail_z_ligature, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_eth to IpaSymbol(R.string.symbol_eth, R.string.eth, R.string.descr_eth, R.string.explanation_eth, R.raw.dental_fricative_voiced_mri, 500, R.raw.dental_fricative_voiced_animated, 500),
        R.string.symbol_lower_case_e to IpaSymbol(R.string.symbol_lower_case_e, R.string.lower_case_e, R.string.descr_lower_case_e, R.string.explanation_lower_case_e, R.raw.front_closemid_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_schwa to IpaSymbol(R.string.symbol_schwa, R.string.schwa, R.string.descr_schwa, R.string.explanation_schwa, R.raw.central_mid_mri, 500, R.string.empty_string, 500),
        R.string.symbol_superscript_schwa to IpaSymbol(R.string.symbol_superscript_schwa, R.string.superscript_schwa, R.string.descr_superscript_schwa, R.string.explanation_superscript_schwa, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_right_hook_schwa to IpaSymbol(R.string.symbol_right_hook_schwa, R.string.right_hook_schwa, R.string.descr_right_hook_schwa, R.string.explanation_right_hook_schwa, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_reversed_e to IpaSymbol(R.string.symbol_reversed_e, R.string.reversed_e, R.string.descr_reversed_e, R.string.explanation_reversed_e, R.raw.central_closemid_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_epsilon to IpaSymbol(R.string.symbol_epsilon, R.string.epsilon, R.string.descr_epsilon, R.string.explanation_epsilon, R.raw.front_openmid_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_closed_epsilon to IpaSymbol(R.string.symbol_closed_epsilon, R.string.closed_epsilon, R.string.descr_closed_epsilon, R.string.explanation_closed_epsilon, R.raw.central_openmid_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_reversed_epsilon to IpaSymbol(R.string.symbol_reversed_epsilon, R.string.reversed_epsilon, R.string.descr_reversed_epsilon, R.string.explanation_reversed_epsilon, R.raw.central_openmid_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_closed_reversed_epsilon to IpaSymbol(R.string.symbol_closed_reversed_epsilon, R.string.closed_reversed_epsilon, R.string.descr_closed_reversed_epsilon, R.string.explanation_closed_reversed_epsilon, R.raw.central_openmid_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_f to IpaSymbol(R.string.symbol_lower_case_f, R.string.lower_case_f, R.string.descr_lower_case_f, R.string.explanation_lower_case_f, R.raw.labiodental_fricative_voiceless_mri, 500, R.raw.labiodental_fricative_voiceless_animated, 500),
        R.string.symbol_opentail_g to IpaSymbol(R.string.symbol_opentail_g, R.string.opentail_g, R.string.descr_opentail_g, R.string.explanation_opentail_g, R.raw.velar_plosive_voiced_mri, 500, R.raw.velar_plosive_voiced_animated, 500),
        R.string.symbol_hooktop_g to IpaSymbol(R.string.symbol_hooktop_g, R.string.hooktop_g, R.string.descr_hooktop_g, R.string.explanation_hooktop_g, R.raw.velar_voiced_implosive_mri, 500, R.string.empty_string, 500),
        R.string.symbol_looptail_g to IpaSymbol(R.string.symbol_looptail_g, R.string.looptail_g, R.string.descr_looptail_g, R.string.explanation_looptail_g, R.raw.velar_plosive_voiced_mri, 500, R.raw.velar_plosive_voiced_animated, 500),
        R.string.symbol_small_capital_g to IpaSymbol(R.string.symbol_small_capital_g, R.string.small_capital_g, R.string.descr_small_capital_g, R.string.explanation_small_capital_g, R.raw.uvular_plosive_voiced_mri, 500, R.raw.uvular_plosive_voiced_animated, 500),
        R.string.symbol_hooktop_small_capital_g to IpaSymbol(R.string.symbol_hooktop_small_capital_g, R.string.hooktop_small_capital_g, R.string.descr_hooktop_small_capital_g, R.string.explanation_hooktop_small_capital_g, R.raw.uvular_voiced_implosive_mri, 500, R.string.empty_string, 500),
        R.string.symbol_gamma to IpaSymbol(R.string.symbol_gamma, R.string.gamma, R.string.descr_gamma, R.string.explanation_gamma, R.raw.velar_fricative_voiced_mri, 500, R.raw.velar_fricative_voiced_animated, 500),
        R.string.symbol_superscript_gamma to IpaSymbol(R.string.symbol_superscript_gamma, R.string.superscript_gamma, R.string.descr_superscript_gamma, R.string.explanation_superscript_gamma, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_rams_horns to IpaSymbol(R.string.symbol_rams_horns, R.string.rams_horns, R.string.descr_rams_horns, R.string.explanation_rams_horns, R.raw.back_closemid_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_h to IpaSymbol(R.string.symbol_lower_case_h, R.string.lower_case_h, R.string.descr_lower_case_h, R.string.explanation_lower_case_h, R.raw.glottal_fricative_voiceless_mri, 500, R.raw.glottal_fricative_voiceless_animated, 500),
        R.string.symbol_superscript_h to IpaSymbol(R.string.symbol_superscript_h, R.string.superscript_h, R.string.descr_superscript_h, R.string.explanation_superscript_h, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_barred_h to IpaSymbol(R.string.symbol_barred_h, R.string.barred_h, R.string.descr_barred_h, R.string.explanation_barred_h, R.raw.pharyngeal_fricative_voiceless_mri, 500, R.raw.pharyngeal_fricative_voiceless_animated, 500),
        R.string.symbol_hooktop_h to IpaSymbol(R.string.symbol_hooktop_h, R.string.hooktop_h, R.string.descr_hooktop_h, R.string.explanation_hooktop_h, R.raw.glottal_fricative_voiced_mri, 500, R.raw.glottal_fricative_voiced_animated, 500),
        R.string.symbol_hooktop_heng to IpaSymbol(R.string.symbol_hooktop_heng, R.string.hooktop_heng, R.string.descr_hooktop_heng, R.string.explanation_hooktop_heng, R.raw.simultaneous_voiceless_plostalveolar_and_velar_fricative_mri, 500, R.string.empty_string, 500),
        R.string.symbol_turned_h to IpaSymbol(R.string.symbol_turned_h, R.string.turned_h, R.string.descr_turned_h, R.string.explanation_turned_h, R.raw.labial_palatal_approximant_voiced_mri, 500, R.string.empty_string, 500),
        R.string.symbol_small_capital_h to IpaSymbol(R.string.symbol_small_capital_h, R.string.small_capital_h, R.string.descr_small_capital_h, R.string.explanation_small_capital_h, R.raw.epiglottal_fricative_voiceless_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_i to IpaSymbol(R.string.symbol_lower_case_i, R.string.lower_case_i, R.string.descr_lower_case_i, R.string.explanation_lower_case_i, R.raw.front_close_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_undotted_i to IpaSymbol(R.string.symbol_undotted_i, R.string.undotted_i, R.string.descr_undotted_i, R.string.explanation_undotted_i, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_barred_i to IpaSymbol(R.string.symbol_barred_i, R.string.barred_i, R.string.descr_barred_i, R.string.explanation_barred_i, R.raw.central_close_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_iota to IpaSymbol(R.string.symbol_iota, R.string.iota, R.string.descr_iota, R.string.explanation_iota, R.raw.nearfront_nearclose_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_small_capital_i to IpaSymbol(R.string.symbol_small_capital_i, R.string.small_capital_i, R.string.descr_small_capital_i, R.string.explanation_small_capital_i, R.raw.nearfront_nearclose_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_j to IpaSymbol(R.string.symbol_lower_case_j, R.string.lower_case_j, R.string.descr_lower_case_j, R.string.explanation_lower_case_j, R.raw.palatal_approximant_mri, 500, R.raw.palatal_approximant_animated, 500),
        R.string.symbol_superscript_j to IpaSymbol(R.string.symbol_superscript_j, R.string.superscript_j, R.string.descr_superscript_j, R.string.explanation_superscript_j, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_curly_tail_j to IpaSymbol(R.string.symbol_curly_tail_j, R.string.curly_tail_j, R.string.descr_curly_tail_j, R.string.explanation_curly_tail_j, R.raw.palatal_fricative_voiced_mri, 500, R.raw.palatal_fricative_voiced_animated, 500),
        R.string.symbol_j_wedge to IpaSymbol(R.string.symbol_j_wedge, R.string.j_wedge, R.string.descr_j_wedge, R.string.explanation_j_wedge, R.raw.postalveolar_affricate_voiced_mri, 500, R.string.empty_string, 500),
        R.string.symbol_barred_dotless_j to IpaSymbol(R.string.symbol_barred_dotless_j, R.string.barred_dotless_j, R.string.descr_barred_dotless_j, R.string.explanation_barred_dotless_j, R.raw.palatal_plosive_voiced_mri, 500, R.raw.palatal_plosive_voiced_animated, 500),
        R.string.symbol_hooktop_barred_dotless_j to IpaSymbol(R.string.symbol_hooktop_barred_dotless_j, R.string.hooktop_barred_dotless_j, R.string.descr_hooktop_barred_dotless_j, R.string.explanation_hooktop_barred_dotless_j, R.raw.palatal_voiced_implosive_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_k to IpaSymbol(R.string.symbol_lower_case_k, R.string.lower_case_k, R.string.descr_lower_case_k, R.string.explanation_lower_case_k, R.raw.velar_plosive_voiceless_mri, 500, R.raw.velar_plosive_voiceless_animated, 500),
        R.string.symbol_hooktop_k to IpaSymbol(R.string.symbol_hooktop_k, R.string.hooktop_k, R.string.descr_hooktop_k, R.string.explanation_hooktop_k, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_l to IpaSymbol(R.string.symbol_lower_case_l, R.string.lower_case_l, R.string.descr_lower_case_l, R.string.explanation_lower_case_l, R.raw.alveolar_lateral_approximant_mri, 500, R.raw.alveolar_lateral_approximant_animated, 500),
        R.string.symbol_superscript_l to IpaSymbol(R.string.symbol_superscript_l, R.string.superscript_l, R.string.descr_superscript_l, R.string.explanation_superscript_l, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_l_with_tilde to IpaSymbol(R.string.symbol_l_with_tilde, R.string.l_with_tilde, R.string.descr_l_with_tilde, R.string.explanation_l_with_tilde, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_belted_l to IpaSymbol(R.string.symbol_belted_l, R.string.belted_l, R.string.descr_belted_l, R.string.explanation_belted_l, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_right_tail_l to IpaSymbol(R.string.symbol_right_tail_l, R.string.right_tail_l, R.string.descr_right_tail_l, R.string.explanation_right_tail_l, R.raw.retroflex_lateral_approximant_mri, 500, R.raw.retroflex_lateral_approximant_animated, 500),
        R.string.symbol_small_capital_l to IpaSymbol(R.string.symbol_small_capital_l, R.string.small_capital_l, R.string.descr_small_capital_l, R.string.explanation_small_capital_l, R.raw.velar_lateral_approximant_mri, 500, R.string.empty_string, 500),
        R.string.symbol_l_ezh_ligature to IpaSymbol(R.string.symbol_l_ezh_ligature, R.string.l_ezh_ligature, R.string.descr_l_ezh_ligature, R.string.explanation_l_ezh_ligature, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_lambda to IpaSymbol(R.string.symbol_lambda, R.string.lambda, R.string.descr_lambda, R.string.explanation_lambda, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_barred_lambda to IpaSymbol(R.string.symbol_barred_lambda, R.string.barred_lambda, R.string.descr_barred_lambda, R.string.explanation_barred_lambda, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_m to IpaSymbol(R.string.symbol_lower_case_m, R.string.lower_case_m, R.string.descr_lower_case_m, R.string.explanation_lower_case_m, R.raw.bilabial_nasal_mri, 500, R.raw.bilabial_nasal_animated, 500),
        R.string.symbol_left_tail_m_at_right to IpaSymbol(R.string.symbol_left_tail_m_at_right, R.string.left_tail_m_at_right, R.string.descr_left_tail_m_at_right, R.string.explanation_left_tail_m_at_right, R.raw.labiodental_nasal_mri, 500, R.raw.labiodental_nasal_animated, 500),
        R.string.symbol_turned_m to IpaSymbol(R.string.symbol_turned_m, R.string.turned_m, R.string.descr_turned_m, R.string.explanation_turned_m, R.raw.back_close_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_turned_m_right_leg to IpaSymbol(R.string.symbol_turned_m_right_leg, R.string.turned_m_right_leg, R.string.descr_turned_m_right_leg, R.string.explanation_turned_m_right_leg, R.raw.velar_approximant_mri, 500, R.raw.velar_approximant_animated, 500),
        R.string.symbol_lower_case_n to IpaSymbol(R.string.symbol_lower_case_n, R.string.lower_case_n, R.string.descr_lower_case_n, R.string.explanation_lower_case_n, R.raw.alveolar_nasal_mri, 500, R.raw.alveolar_nasal_animated, 500),
        R.string.symbol_superscript_n to IpaSymbol(R.string.symbol_superscript_n, R.string.superscript_n, R.string.descr_superscript_n, R.string.explanation_superscript_n, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_n_right_leg to IpaSymbol(R.string.symbol_n_right_leg, R.string.n_right_leg, R.string.descr_n_right_leg, R.string.explanation_n_right_leg, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_left_tail_n_at_left to IpaSymbol(R.string.symbol_left_tail_n_at_left, R.string.left_tail_n_at_left, R.string.descr_left_tail_n_at_left, R.string.explanation_left_tail_n_at_left, R.raw.palatal_nasal_mri, 500, R.raw.palatal_nasal_animated, 500),
        R.string.symbol_eng to IpaSymbol(R.string.symbol_eng, R.string.eng, R.string.descr_eng, R.string.explanation_eng, R.raw.velar_nasal_mri, 500, R.raw.velar_nasal_animated, 500),
        R.string.symbol_right_tail_n to IpaSymbol(R.string.symbol_right_tail_n, R.string.right_tail_n, R.string.descr_right_tail_n, R.string.explanation_right_tail_n, R.raw.retroflex_nasal_mri, 500, R.raw.retroflex_nasal_animated, 500),
        R.string.symbol_small_capital_n to IpaSymbol(R.string.symbol_small_capital_n, R.string.small_capital_n, R.string.descr_small_capital_n, R.string.explanation_small_capital_n, R.raw.uvular_nasal_mri, 500, R.raw.uvular_nasal_animated, 500),
        R.string.symbol_lower_case_o to IpaSymbol(R.string.symbol_lower_case_o, R.string.lower_case_o, R.string.descr_lower_case_o, R.string.explanation_lower_case_o, R.raw.back_closemid_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_bulls_eye to IpaSymbol(R.string.symbol_bulls_eye, R.string.bulls_eye, R.string.descr_bulls_eye, R.string.explanation_bulls_eye, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_barred_o to IpaSymbol(R.string.symbol_barred_o, R.string.barred_o, R.string.descr_barred_o, R.string.explanation_barred_o, R.raw.central_closemid_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_slashed_o to IpaSymbol(R.string.symbol_slashed_o, R.string.slashed_o, R.string.descr_slashed_o, R.string.explanation_slashed_o, R.raw.front_closemid_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_o_e_ligature to IpaSymbol(R.string.symbol_lower_case_o_e_ligature, R.string.lower_case_o_e_ligature, R.string.descr_lower_case_o_e_ligature, R.string.explanation_lower_case_o_e_ligature, R.raw.front_openmid_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_small_capital_o_e_ligature to IpaSymbol(R.string.symbol_small_capital_o_e_ligature, R.string.small_capital_o_e_ligature, R.string.descr_small_capital_o_e_ligature, R.string.explanation_small_capital_o_e_ligature, R.raw.front_open_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_open_o to IpaSymbol(R.string.symbol_open_o, R.string.open_o, R.string.descr_open_o, R.string.explanation_open_o, R.raw.back_openmid_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_closed_omega to IpaSymbol(R.string.symbol_closed_omega, R.string.closed_omega, R.string.descr_closed_omega, R.string.explanation_closed_omega, R.raw.nearback_nearclose_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_p to IpaSymbol(R.string.symbol_lower_case_p, R.string.lower_case_p, R.string.descr_lower_case_p, R.string.explanation_lower_case_p, R.raw.bilabial_plosive_voiceless_mri, 500, R.raw.bilabial_plosive_voiceless_animated, 500),
        R.string.symbol_hooktop_p to IpaSymbol(R.string.symbol_hooktop_p, R.string.hooktop_p, R.string.descr_hooktop_p, R.string.explanation_hooktop_p, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_phi to IpaSymbol(R.string.symbol_phi, R.string.phi, R.string.descr_phi, R.string.explanation_phi, R.raw.bilabial_fricative_voiceless_mri, 500, R.raw.bilabial_fricative_voiceless_animated, 500),
        R.string.symbol_lower_case_q to IpaSymbol(R.string.symbol_lower_case_q, R.string.lower_case_q, R.string.descr_lower_case_q, R.string.explanation_lower_case_q, R.raw.uvular_plosive_voiceless_mri, 500, R.raw.uvular_plosive_voiceless_animated, 500),
        R.string.symbol_hooktop_q to IpaSymbol(R.string.symbol_hooktop_q, R.string.hooktop_q, R.string.descr_hooktop_q, R.string.explanation_hooktop_q, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_r to IpaSymbol(R.string.symbol_lower_case_r, R.string.lower_case_r, R.string.descr_lower_case_r, R.string.explanation_lower_case_r, R.raw.alveolar_trill_mri, 500, R.raw.alveolar_trill_animated, 500),
        R.string.symbol_fish_hook_r to IpaSymbol(R.string.symbol_fish_hook_r, R.string.fish_hook_r, R.string.descr_fish_hook_r, R.string.explanation_fish_hook_r, R.raw.alveolar_flap_mri, 500, R.raw.alveolar_flap_animated, 500),
        R.string.symbol_long_leg_r to IpaSymbol(R.string.symbol_long_leg_r, R.string.long_leg_r, R.string.descr_long_leg_r, R.string.explanation_long_leg_r, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_turned_long_leg_r to IpaSymbol(R.string.symbol_turned_long_leg_r, R.string.turned_long_leg_r, R.string.descr_turned_long_leg_r, R.string.explanation_turned_long_leg_r, R.raw.alveolar_lateral_flap_voiced_mri, 500, R.string.empty_string, 500),
        R.string.symbol_right_tail_r to IpaSymbol(R.string.symbol_right_tail_r, R.string.right_tail_r, R.string.descr_right_tail_r, R.string.explanation_right_tail_r, R.raw.retroflex_flap_mri, 500, R.raw.retroflex_flap_animated, 500),
        R.string.symbol_turned_r to IpaSymbol(R.string.symbol_turned_r, R.string.turned_r, R.string.descr_turned_r, R.string.explanation_turned_r, R.raw.alveolar_approximant_mri, 500, R.raw.alveolar_approximant_animated, 500),
        R.string.symbol_turned_r_right_tail to IpaSymbol(R.string.symbol_turned_r_right_tail, R.string.turned_r_right_tail, R.string.descr_turned_r_right_tail, R.string.explanation_turned_r_right_tail, R.raw.retroflex_approximant_mri, 500, R.raw.retroflex_approximant_animated, 500),
        R.string.symbol_small_capital_r to IpaSymbol(R.string.symbol_small_capital_r, R.string.small_capital_r, R.string.descr_small_capital_r, R.string.explanation_small_capital_r, R.raw.uvular_trill_mri, 500, R.raw.uvular_trill_animated, 500),
        R.string.symbol_inverted_small_capital_r to IpaSymbol(R.string.symbol_inverted_small_capital_r, R.string.inverted_small_capital_r, R.string.descr_inverted_small_capital_r, R.string.explanation_inverted_small_capital_r, R.raw.uvular_fricative_voiced_mri, 500, R.raw.uvular_fricative_voiced_animated, 500),
        R.string.symbol_lower_case_s to IpaSymbol(R.string.symbol_lower_case_s, R.string.lower_case_s, R.string.descr_lower_case_s, R.string.explanation_lower_case_s, R.raw.alveolar_fricative_voiceless_mri, 500, R.raw.alveolar_fricative_voiceless_animated, 500),
        R.string.symbol_s_wedge to IpaSymbol(R.string.symbol_s_wedge, R.string.s_wedge, R.string.descr_s_wedge, R.string.explanation_s_wedge, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_right_tail_s_at_left to IpaSymbol(R.string.symbol_right_tail_s_at_left, R.string.right_tail_s_at_left, R.string.descr_right_tail_s_at_left, R.string.explanation_right_tail_s_at_left, R.raw.retroflex_fricative_voiceless_mri, 500, R.raw.retroflex_fricative_voiceless_animated, 500),
        R.string.symbol_esh to IpaSymbol(R.string.symbol_esh, R.string.esh, R.string.descr_esh, R.string.explanation_esh, R.raw.postalveolar_fricative_voiceless_mri, 500, R.raw.postalveolar_fricative_voiceless_animated, 500),
        R.string.symbol_lower_case_t to IpaSymbol(R.string.symbol_lower_case_t, R.string.lower_case_t, R.string.descr_lower_case_t, R.string.explanation_lower_case_t, R.raw.alveolar_plosive_voiceless_mri, 500, R.raw.alveolar_plosive_voiceless_animated, 500),
        R.string.symbol_hooktop_t to IpaSymbol(R.string.symbol_hooktop_t, R.string.hooktop_t, R.string.descr_hooktop_t, R.string.explanation_hooktop_t, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_right_tail_t to IpaSymbol(R.string.symbol_right_tail_t, R.string.right_tail_t, R.string.descr_right_tail_t, R.string.explanation_right_tail_t, R.raw.retroflex_plosive_voiceless_mri, 500, R.raw.retroflex_plosive_voiceless_animated, 500),
        R.string.symbol_theta to IpaSymbol(R.string.symbol_theta, R.string.theta, R.string.descr_theta, R.string.explanation_theta, R.raw.dental_fricative_voiceless_mri, 500, R.raw.dental_fricative_voiceless_animated, 500),
        R.string.symbol_superscript_theta to IpaSymbol(R.string.symbol_superscript_theta, R.string.superscipt_theta, R.string.descr_superscript_theta, R.string.explanation_superscript_theta, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_u to IpaSymbol(R.string.symbol_lower_case_u, R.string.lower_case_u, R.string.descr_lower_case_u, R.string.explanation_lower_case_u, R.raw.back_close_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_barred_u to IpaSymbol(R.string.symbol_barred_u, R.string.barred_u, R.string.descr_barred_u, R.string.explanation_barred_u, R.raw.central_close_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_upsilon to IpaSymbol(R.string.symbol_upsilon, R.string.upsilon, R.string.descr_upsilon, R.string.explanation_upsilon, R.raw.nearback_nearclose_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_v to IpaSymbol(R.string.symbol_lower_case_v, R.string.lower_case_v, R.string.descr_lower_case_v, R.string.explanation_lower_case_v, R.raw.labiodental_fricative_voiced_mri, 500, R.raw.labiodental_fricative_voiced_animated, 500),
        R.string.symbol_cursive_v to IpaSymbol(R.string.symbol_cursive_v, R.string.cursive_v, R.string.descr_cursive_v, R.string.explanation_cursive_v, R.raw.labiodental_approximant_mri, 500, R.raw.labiodental_approximant_animated, 500),
        R.string.symbol_turned_v to IpaSymbol(R.string.symbol_turned_v, R.string.turned_v, R.string.descr_turned_v, R.string.explanation_turned_v, R.raw.back_openmid_unrounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_w to IpaSymbol(R.string.symbol_lower_case_w, R.string.lower_case_w, R.string.descr_lower_case_w, R.string.explanation_lower_case_w, R.raw.labial_velar_approximant_voiced_mri, 500, R.string.empty_string, 500),
        R.string.symbol_superscript_w to IpaSymbol(R.string.symbol_superscript_w, R.string.superscript_w, R.string.descr_superscript_w, R.string.explanation_superscript_w, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_turned_w to IpaSymbol(R.string.symbol_turned_w, R.string.turned_w, R.string.descr_turned_w, R.string.explanation_turned_w, R.raw.labial_velar_fricative_voiceless_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_x to IpaSymbol(R.string.symbol_lower_case_x, R.string.lower_case_x, R.string.descr_lower_case_x, R.string.explanation_lower_case_x, R.raw.velar_fricative_voiceless_mri, 500, R.raw.velar_fricative_voiceless_animated, 500),
        R.string.symbol_superscript_x to IpaSymbol(R.string.symbol_superscript_x, R.string.superscript_x, R.string.descr_superscript_x, R.string.explanation_superscript_x, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_chi to IpaSymbol(R.string.symbol_chi, R.string.chi, R.string.descr_chi, R.string.explanation_chi, R.raw.uvular_fricative_voiceless_mri, 500, R.raw.uvular_fricative_voiceless_animated, 500),
        R.string.symbol_lower_case_y to IpaSymbol(R.string.symbol_lower_case_y, R.string.lower_case_y, R.string.descr_lower_case_y, R.string.explanation_lower_case_y, R.raw.front_close_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_turned_y to IpaSymbol(R.string.symbol_turned_y, R.string.turned_y, R.string.descr_turned_y, R.string.explanation_turned_y, R.raw.palatal_lateral_approximant_mri, 500, R.raw.palatal_lateral_approximant_animated, 500),
        R.string.symbol_small_capital_y to IpaSymbol(R.string.symbol_small_capital_y, R.string.small_capital_y, R.string.descr_small_capital_y, R.string.explanation_small_capital_y, R.raw.nearfront_nearclose_rounded_mri, 500, R.string.empty_string, 500),
        R.string.symbol_lower_case_z to IpaSymbol(R.string.symbol_lower_case_z, R.string.lower_case_z, R.string.descr_lower_case_z, R.string.explanation_lower_case_z, R.raw.alveolar_fricative_voiced_mri, 500, R.raw.alveolar_fricative_voiced_animated, 500),
        R.string.symbol_z_wedge to IpaSymbol(R.string.symbol_z_wedge, R.string.z_wedge, R.string.descr_z_wedge, R.string.explanation_z_wedge, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_curly_tail_z to IpaSymbol(R.string.symbol_curly_tail_z, R.string.curly_tail_z, R.string.descr_curly_tail_z, R.string.explanation_curly_tail_z, R.raw.alveolopalatal_fricative_voiced_mri, 500, R.string.empty_string, 500),
        R.string.symbol_right_tail_z to IpaSymbol(R.string.symbol_right_tail_z, R.string.right_tail_z, R.string.descr_right_tail_z, R.string.explanation_right_tail_z, R.raw.retroflex_fricative_voiced_mri, 500, R.raw.retroflex_fricative_voiced_animated, 500),
        R.string.symbol_ezh_tailed_z to IpaSymbol(R.string.symbol_ezh_tailed_z, R.string.ezh_tailed_z, R.string.descr_ezh_tailed_z, R.string.explanation_ezh_tailed_z, R.raw.postalveolar_fricative_voiced_mri, 500, R.raw.postalveolar_fricative_voiced_animated, 500),
        R.string.symbol_curly_tail_ezh to IpaSymbol(R.string.symbol_curly_tail_ezh, R.string.curly_tail_ezh, R.string.descr_curly_tail_ezh, R.string.explanation_curly_tail_ezh, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_barred_two to IpaSymbol(R.string.symbol_barred_two, R.string.barred_two, R.string.descr_barred_two, R.string.explanation_barred_two, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_glottal_stop to IpaSymbol(R.string.symbol_glottal_stop, R.string.glottal_stop, R.string.descr_glottal_stop, R.string.explanation_glottal_stop, R.raw.glottal_plosive_mri, 500, R.raw.glottal_plosive_animated, 500),
        R.string.symbol_barred_glottal_stop to IpaSymbol(R.string.symbol_barred_glottal_stop, R.string.barred_glottal_stop, R.string.descr_barred_glottal_stop, R.string.explanation_barred_glottal_stop, R.raw.epiglottal_plosive_mri, 500, R.string.empty_string, 500),
        R.string.symbol_inverted_glottal_stop to IpaSymbol(R.string.symbol_inverted_glottal_stop, R.string.inverted_glottal_stop, R.string.descr_inverted_glottal_stop, R.string.explanation_inverted_glottal_stop, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_reversed_glottal_stop to IpaSymbol(R.string.symbol_reversed_glottal_stop, R.string.reversed_glottal_stop, R.string.descr_reversed_glottal_stop, R.string.explanation_reversed_glottal_stop, R.raw.pharyngeal_fricative_voiced_mri, 500, R.raw.pharyngeal_fricative_voiced_animated, 500),
        R.string.symbol_superscript_reversed_glottal_stop to IpaSymbol(R.string.symbol_superscript_reversed_glottal_stop, R.string.superscript_reversed_glottal_stop, R.string.descr_superscript_reversed_glottal_stop, R.string.explanation_superscript_reversed_glottal_stop, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_barred_reversed_glottal_stop to IpaSymbol(R.string.symbol_barred_reversed_glottal_stop, R.string.barred_reversed_glottal_stop, R.string.descr_barred_reversed_glottal_stop, R.string.explanation_barred_reversed_glottal_stop, R.raw.epiglottal_fricative_voiced_mri, 500, R.string.empty_string, 500),
        R.string.symbol_bulls_eye to IpaSymbol(R.string.symbol_bulls_eye, R.string.bulls_eye, R.string.descr_bulls_eye, R.string.explanation_bulls_eye, R.raw.bilabial_click_mri, 500, R.string.empty_string, 500),
        R.string.symbol_pipe to IpaSymbol(R.string.symbol_pipe, R.string.pipe, R.string.descr_pipe, R.string.explanation_pipe, R.raw.dental_click_mri, 500, R.string.empty_string, 500),
        R.string.symbol_double_barred_pipe to IpaSymbol(R.string.symbol_double_barred_pipe, R.string.double_barred_pipe, R.string.descr_double_barred_pipe, R.string.explanation_double_barred_pipe, R.raw.palatoalveolar_click_mri, 500, R.string.empty_string, 500),
        R.string.symbol_double_pipe to IpaSymbol(R.string.symbol_double_pipe, R.string.double_pipe, R.string.descr_double_pipe, R.string.explanation_double_pipe, R.raw.alveolar_lateral_click_mri, 500, R.string.empty_string, 500),
        R.string.symbol_exclamation_point to IpaSymbol(R.string.symbol_exclamation_point, R.string.exclamation_point, R.string.descr_exclamation_point, R.string.explanation_exclamation_point, R.raw.alveolar_click_mri, 500, R.string.empty_string, 500),
        R.string.symbol_bilabial_ejective to IpaSymbol(R.string.symbol_bilabial_ejective, R.string.bilabial_ejective, R.string.descr_bilabial_ejective, R.string.explanation_bilabial_ejective, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_dental_ejective to IpaSymbol(R.string.symbol_dental_ejective, R.string.dental_ejective, R.string.descr_dental_ejective, R.string.explanation_dental_ejective, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_velar_ejective to IpaSymbol(R.string.symbol_velar_ejective, R.string.velar_ejective, R.string.descr_velar_ejective, R.string.explanation_velar_ejective, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_alveolar_fricative_ejective to IpaSymbol(R.string.symbol_alveolar_fricative_ejective, R.string.alveolar_fricative_ejective, R.string.descr_alveolar_fricative_ejective, R.string.explanation_alveolar_fricative_ejective, R.string.empty_string, 500, R.string.empty_string, 500),

        R.string.example_voiceless to IpaSymbol(R.string.example_voiceless, R.string.under_ring, R.string.descr_under_ring, R.string.explanation_under_ring, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_voiced to IpaSymbol(R.string.example_voiced, R.string.subscript_wedge, R.string.descr_subscript_wedge, R.string.explanation_subscript_wedge, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_aspirated to IpaSymbol(R.string.example_aspirated, R.string.superscript_h, R.string.descr_superscript_h, R.string.descr_superscript_h, R.string.explanation_superscript_h, 500, R.string.empty_string, 500),
        R.string.example_more_rounded to IpaSymbol(R.string.example_more_rounded, R.string.subscript_right_half_ring, R.string.descr_subscript_right_half_ring, R.string.explanation_subscript_right_half_ring, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_less_rounded to IpaSymbol(R.string.example_less_rounded, R.string.subscript_left_half_ring, R.string.descr_subscript_left_half_ring, R.string.explanation_subscript_left_half_ring, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_advanced to IpaSymbol(R.string.example_advanced, R.string.subscript_plus, R.string.descr_subscript_plus, R.string.explanation_subscript_plus, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_retracted to IpaSymbol(R.string.example_retracted, R.string.under_bar, R.string.descr_under_bar, R.string.explanation_under_bar, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_centralized to IpaSymbol(R.string.example_centralized, R.string.umlaut, R.string.descr_umlaut, R.string.explanation_umlaut, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_mid_centralized to IpaSymbol(R.string.example_mid_centralized, R.string.over_cross, R.string.descr_over_cross, R.string.explanation_over_cross, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_syllabic to IpaSymbol(R.string.example_syllabic, R.string.syllabicity_mark, R.string.descr_syllabicity_mark, R.string.explanation_syllabicity_mark, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_non_syllabic to IpaSymbol(R.string.example_non_syllabic, R.string.subscript_arch, R.string.descr_subscript_arch, R.string.explanation_subscript_arch, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_rhoticity to IpaSymbol(R.string.example_rhoticity, R.string.right_hook, R.string.descr_right_hook, R.string.explanation_right_hook, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_breathy_voiced to IpaSymbol(R.string.example_breathy_voiced, R.string.subscript_umlaut, R.string.descr_subscript_umlaut, R.string.explanation_subscript_umlaut, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_creaky_voiced to IpaSymbol(R.string.example_creaky_voiced, R.string.subscript_tilde, R.string.descr_subscript_tilde, R.string.explanation_subscript_tilde, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_linguolabial to IpaSymbol(R.string.example_linguolabial, R.string.subscript_seagull, R.string.descr_subscript_seagull, R.string.explanation_subscript_seagull, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_labialized to IpaSymbol(R.string.example_labialized, R.string.superscript_w, R.string.descr_superscript_w, R.string.explanation_superscript_w, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_palatalized to IpaSymbol(R.string.example_palatalized, R.string.superscript_j, R.string.descr_superscript_j, R.string.explanation_superscript_j, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_velarized to IpaSymbol(R.string.example_velarized, R.string.superscript_gamma, R.string.descr_superscript_gamma, R.string.explanation_superscript_gamma, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_pharyngealized to IpaSymbol(R.string.example_pharyngealized, R.string.superscript_reversed_glottal_stop, R.string.descr_superscript_reversed_glottal_stop, R.string.explanation_superscript_reversed_glottal_stop, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_velarized_or_pharyngealized to IpaSymbol(R.string.example_velarized_or_pharyngealized, R.string.superimposed_tilde, R.string.descr_superimposed_tilde, R.string.explanation_superimposed_tilde, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_raised to IpaSymbol(R.string.example_raised, R.string.raising_sign, R.string.descr_raising_sign, R.string.explanation_raising_sign, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_lowered to IpaSymbol(R.string.example_lowered, R.string.lowering_sign, R.string.descr_lowering_sign, R.string.explanation_lowering_sign, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_advanced_tongue_root to IpaSymbol(R.string.example_advanced_tongue_root, R.string.advancing_sign, R.string.descr_advancing_sign, R.string.explanation_advancing_sign, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_retracted_tongue_root to IpaSymbol(R.string.example_retracted_tongue_root, R.string.retracting_sign, R.string.descr_retracting_sign, R.string.explanation_retracting_sign, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_dental to IpaSymbol(R.string.example_dental, R.string.subscript_bridge, R.string.descr_subscript_bridge, R.string.explanation_subscript_bridge, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_apical to IpaSymbol(R.string.example_apical, R.string.inverted_subscript_bridge, R.string.descr_inverted_subscript_bridge, R.string.descr_inverted_subscript_bridge, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_laminal to IpaSymbol(R.string.example_laminal, R.string.subscript_square, R.string.descr_subscript_square, R.string.explanation_subscript_square, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_nasalized to IpaSymbol(R.string.example_nasalized, R.string.superscript_tilde, R.string.descr_superscript_tilde, R.string.explanation_superscript_tilde, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_nasal_release to IpaSymbol(R.string.example_nasal_release, R.string.superscript_n, R.string.descr_superscript_n, R.string.explanation_superscript_n, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_lateral_release to IpaSymbol(R.string.example_lateral_release, R.string.superscript_l, R.string.descr_superscript_l, R.string.explanation_superscript_l, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_no_audible_release to IpaSymbol(R.string.example_no_audible_release, R.string.corner, R.string.descr_corner, R.string.explanation_corner, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_primary_stress to IpaSymbol(R.string.example_primary_stress, R.string.vertical_stroke_superior, R.string.descr_vertical_stroke_superior, R.string.explanation_vertical_stroke_superior, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_secondary_stress to IpaSymbol(R.string.example_secondary_stress, R.string.vertical_stroke_inferior, R.string.descr_vertical_stroke_inferior, R.string.explanation_vertical_stroke_inferior, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_long to IpaSymbol(R.string.example_long, R.string.length_mark, R.string.descr_length_mark, R.string.explanation_length_mark, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_half_long to IpaSymbol(R.string.example_half_long, R.string.half_length_mark, R.string.descr_half_length_mark, R.string.explanation_half_length_mark, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_extra_short to IpaSymbol(R.string.example_extra_short, R.string.breve, R.string.descr_breve, R.string.explanation_breve, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_minor_group to IpaSymbol(R.string.example_minor_group, R.string.vertical_line, R.string.descr_vertical_line, R.string.explanation_vertical_line, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_major_group to IpaSymbol(R.string.example_major_group, R.string.double_vertical_line, R.string.descr_double_vertical_line, R.string.explanation_double_vertical_line, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_syllable_break to IpaSymbol(R.string.example_syllable_break, R.string.period, R.string.descr_period, R.string.explanation_period, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_linking to IpaSymbol(R.string.example_linking, R.string.bottom_tie_bar, R.string.descr_bottom_tie_bar, R.string.explanation_bottom_tie_bar, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_extra_high to IpaSymbol(R.string.example_level_extra_high, R.string.double_acute_accent, R.string.descr_double_acute_accent, R.string.explanation_double_acute_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_high to IpaSymbol(R.string.example_level_high, R.string.acute_accent, R.string.descr_acute_accent, R.string.explanation_acute_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_mid to IpaSymbol(R.string.example_level_mid, R.string.macron, R.string.descr_macron, R.string.explanation_macron, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_low to IpaSymbol(R.string.example_level_low, R.string.grave_accent, R.string.descr_grave_accent, R.string.explanation_grave_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_extra_low to IpaSymbol(R.string.example_level_extra_low, R.string.double_grave_accent, R.string.descr_double_grave_accent, R.string.explanation_double_grave_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_extra_high_2 to IpaSymbol(R.string.example_level_extra_high_2, R.string.extra_high_tone_letter, R.string.descr_double_acute_accent, R.string.explanation_double_acute_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_high_2 to IpaSymbol(R.string.example_level_high_2, R.string.high_tone_letter, R.string.descr_acute_accent, R.string.explanation_acute_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_mid_2 to IpaSymbol(R.string.example_level_mid_2, R.string.mid_tone_letter, R.string.descr_macron, R.string.explanation_macron, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_low_2 to IpaSymbol(R.string.example_level_low_2, R.string.low_tone_letter, R.string.descr_grave_accent, R.string.explanation_grave_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_extra_low_2 to IpaSymbol(R.string.example_level_extra_low_2, R.string.extra_low_tone_letter, R.string.descr_double_grave_accent, R.string.explanation_double_grave_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_down_arrow to IpaSymbol(R.string.symbol_down_arrow, R.string.down_arrow, R.string.descr_down_arrow, R.string.explanation_down_arrow, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_up_arrow to IpaSymbol(R.string.symbol_up_arrow, R.string.up_arrow, R.string.descr_up_arrow, R.string.explanation_up_arrow, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_downstep to IpaSymbol(R.string.example_level_downstep, R.string.downstep, R.string.descr_down_arrow, R.string.explanation_down_arrow, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_level_upstep to IpaSymbol(R.string.example_level_upstep, R.string.upstep, R.string.descr_up_arrow, R.string.explanation_up_arrow, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_rising to IpaSymbol(R.string.example_contour_rising, R.string.rising, R.string.descr_wedge_hacek, R.string.explanation_wedge_hacek, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_falling to IpaSymbol(R.string.example_contour_falling, R.string.falling, R.string.descr_circumflex, R.string.explanation_circumflex, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_high_rising to IpaSymbol(R.string.example_contour_high_rising, R.string.high_rising, R.string.descr_macron_plus_acute_accent, R.string.explanation_macron_plus_acute_accent, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_low_rising to IpaSymbol(R.string.example_contour_low_rising, R.string.low_rising, R.string.descr_grave_accent_plus_macon, R.string.explanation_grave_accent_plus_macron, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_rising_falling to IpaSymbol(R.string.example_contour_rising_falling, R.string.rising_falling, R.string.descr_contour_rising_falling, R.string.explanation_contour_rising_falling, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_rising_2 to IpaSymbol(R.string.example_contour_rising_2, R.string.rising, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_falling_2 to IpaSymbol(R.string.example_contour_falling_2, R.string.falling, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_high_rising_2 to IpaSymbol(R.string.example_contour_high_rising_2, R.string.high_rising, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_low_rising_2 to IpaSymbol(R.string.example_contour_low_rising_2, R.string.low_rising, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_rising_falling_2 to IpaSymbol(R.string.example_contour_rising_falling_2, R.string.rising_falling, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_global_rise to IpaSymbol(R.string.example_contour_global_rise, R.string.global_rise, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.example_contour_global_fall to IpaSymbol(R.string.example_contour_global_fall, R.string.global_fall, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_downward_diagonal_arrow to IpaSymbol(R.string.symbol_downward_diagonal_arrow, R.string.downward_diagonal_arrow, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500),
        R.string.symbol_upward_diagonal_arrow to IpaSymbol(R.string.symbol_upward_diagonal_arrow, R.string.upward_diagonal_arrow, R.string.empty_string, R.string.empty_string, R.string.empty_string, 500, R.string.empty_string, 500)
        )

data class ConsonantsPulmonicData(
    val sections: List<Int> = listOf(
            R.string.plosive,
            R.string.nasal,
            R.string.trill,
            R.string.tap_or_flap,
            R.string.fricative,
            R.string.lateral_fricative,
            R.string.approximant,
            R.string.lateral_approximant
    ),

    val plosives: List<List<Int>> = listOf(
            listOf(R.string.symbol_lower_case_p, R.string.symbol_lower_case_b),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.symbol_lower_case_t, R.string.symbol_lower_case_d),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.symbol_right_tail_t, R.string.symbol_right_tail_d),
            listOf(R.string.symbol_lower_case_c, R.string.symbol_barred_dotless_j),
            listOf(R.string.symbol_lower_case_k, R.string.symbol_opentail_g),
            listOf(R.string.symbol_lower_case_q, R.string.symbol_small_capital_g),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.symbol_glottal_stop, R.string.empty_string)
    ),

    val nasals: List<List<Int>> = listOf(
            listOf(R.string.empty_string, R.string.symbol_lower_case_m),
            listOf(R.string.empty_string, R.string.symbol_left_tail_m_at_right),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_lower_case_n),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_right_tail_n),
            listOf(R.string.empty_string, R.string.symbol_left_tail_n_at_left),
            listOf(R.string.empty_string, R.string.symbol_eng),
            listOf(R.string.empty_string, R.string.symbol_small_capital_n),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string)
    ),

    val trill: List<List<Int>> = listOf(
            listOf(R.string.empty_string, R.string.symbol_small_capital_b),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_lower_case_r),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_small_capital_r),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string)
    ),

    val tap_or_flap: List<List<Int>> = listOf(
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_fish_hook_r),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_right_tail_r),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string)
    ),

    val fricative: List<List<Int>> = listOf(
            listOf(R.string.symbol_phi, R.string.symbol_beta),
            listOf(R.string.symbol_lower_case_f, R.string.symbol_lower_case_v),
            listOf(R.string.symbol_theta, R.string.symbol_eth),
            listOf(R.string.symbol_lower_case_s, R.string.symbol_lower_case_z),
            listOf(R.string.symbol_esh, R.string.symbol_ezh_tailed_z),
            listOf(R.string.symbol_right_tail_s_at_left, R.string.symbol_right_tail_z),
            listOf(R.string.symbol_c_cedilla, R.string.symbol_curly_tail_j),
            listOf(R.string.symbol_lower_case_x, R.string.symbol_gamma),
            listOf(R.string.symbol_chi, R.string.symbol_inverted_small_capital_r),
            listOf(R.string.symbol_barred_h, R.string.symbol_reversed_glottal_stop),
            listOf(R.string.symbol_lower_case_h, R.string.symbol_hooktop_h)
    ),

    val lateral_fricative: List<List<Int>> = listOf(
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.symbol_belted_l, R.string.symbol_l_ezh_ligature),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string)
    ),

    val approximant: List<List<Int>> = listOf(
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_cursive_v),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_turned_r),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_turned_r_right_tail),
            listOf(R.string.empty_string, R.string.symbol_lower_case_j),
            listOf(R.string.empty_string, R.string.symbol_turned_m_right_leg),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string)
    ),

    val lateral_approximant: List<List<Int>> = listOf(
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_lower_case_l),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.symbol_right_tail_l),
            listOf(R.string.empty_string, R.string.symbol_turned_y),
            listOf(R.string.empty_string, R.string.symbol_small_capital_l),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string),
            listOf(R.string.empty_string, R.string.empty_string)
    ),

    val row_labels: List<Int> = listOf(
            R.string.bilabial,
            R.string.labiodental,
            R.string.dental,
            R.string.alveolar,
            R.string.postalveolar,
            R.string.retroflex,
            R.string.palatal,
            R.string.velar,
            R.string.uvular,
            R.string.pharyngeal,
            R.string.glottal
    ),

    val rows: List<List<List<Int>>> = listOf(
            plosives,
            nasals,
            trill,
            tap_or_flap,
            fricative,
            lateral_fricative,
            approximant,
            lateral_approximant
    )
)

data class ConsonantsNonPulmonicData(
    val sections: List<Int> = listOf(
            R.string.clicks,
            R.string.voiced_implosives,
            R.string.ejectives
    ),

    val clicks: List<Int> = listOf(
            R.string.symbol_bulls_eye,
            R.string.symbol_pipe,
            R.string.symbol_exclamation_point,
            R.string.symbol_double_barred_pipe,
            R.string.symbol_double_pipe
    ),

    val clicks_places_of_articulation: List<Int> = listOf(
            R.string.bilabial,
            R.string.dental,
            R.string._post_alveolar,
            R.string.palatoalveolar,
            R.string.alveolar_lateral
    ),

    val voicedImplosives: List<Int> = listOf(
            R.string.symbol_hooktop_b,
            R.string.symbol_hooktop_d,
            R.string.symbol_hooktop_barred_dotless_j,
            R.string.symbol_hooktop_g,
            R.string.symbol_hooktop_small_capital_g
    ),

    val implosives_places_of_articulation: List<Int> = listOf(
            R.string.bilabial,
            R.string.dental_slash_alveolar,
            R.string.palatal,
            R.string.velar,
            R.string.uvular
    ),

    val ejectives: List<Int> = listOf(
            R.string.symbol_bilabial_ejective,
            R.string.symbol_dental_ejective,
            R.string.symbol_velar_ejective,
            R.string.symbol_alveolar_fricative_ejective
    ),

    val ejectives_places_of_articulation: List<Int> = listOf(
            R.string.bilabial,
            R.string.dental_slash_alveolar,
            R.string.velar,
            R.string.alveolar_fricative
    ),

    val rows: List<List<Int>> = listOf(
            clicks, voicedImplosives, ejectives
    ),

    val row_labels: List<List<Int>> = listOf(
            clicks_places_of_articulation,
            implosives_places_of_articulation,
            ejectives_places_of_articulation
    )
)

data class Diacritics(
    val sections: List<Int> = listOf(
        R.string.diacritics
    ),

    val diacritics: List<Int> = listOf(
        R.string.example_voiceless,
        R.string.example_voiced,
        R.string.example_aspirated,
        R.string.example_more_rounded,
        R.string.example_less_rounded,
        R.string.example_advanced,
        R.string.example_retracted,
        R.string.example_centralized,
        R.string.example_mid_centralized,
        R.string.example_syllabic,
        R.string.example_non_syllabic,
        R.string.example_rhoticity,
        R.string.example_breathy_voiced,
        R.string.example_creaky_voiced,
        R.string.example_linguolabial,
        R.string.example_labialized,
        R.string.example_palatalized,
        R.string.example_velarized,
        R.string.example_pharyngealized,
        R.string.example_velarized_or_pharyngealized,
        R.string.example_raised,
        R.string.example_lowered,
        R.string.example_advanced_tongue_root,
        R.string.example_retracted_tongue_root,
        R.string.example_dental,
        R.string.example_apical,
        R.string.example_laminal,
        R.string.example_nasalized,
        R.string.example_nasal_release,
        R.string.example_lateral_release,
        R.string.example_no_audible_release
    ),

    val places_of_articulation: List<Int> = listOf(
        R.string.voiceless,
        R.string.voiced,
        R.string.aspirated,
        R.string.more_rounded,
        R.string.less_rounded,
        R.string.advanced,
        R.string.retracted,
        R.string.centralized,
        R.string.mid_centralized,
        R.string.syllabic,
        R.string.non_syllabic,
        R.string.rhoticity,
        R.string.breathy_voiced,
        R.string.creaky_voiced,
        R.string.linguolabial,
        R.string.labialized,
        R.string.palatalized,
        R.string.velarized,
        R.string.pharyngealized,
        R.string.velarized_or_pharyngealized,
        R.string.raised,
        R.string.lowered,
        R.string.advanced_tongue_root,
        R.string.retracted_tongue_root,
        R.string.dental,
        R.string.apical,
        R.string.laminal,
        R.string.nasalized,
        R.string.nasal_release,
        R.string.lateral_release,
        R.string.no_audible_release
    )
)

data class Suprasegmentals(

    val sections: List<Int> = listOf(
        R.string.suprasegmentals
    ),

    val suprasegmentals: List<Int> = listOf(
        R.string.example_primary_stress,
        R.string.example_secondary_stress,
        R.string.example_long,
        R.string.example_half_long,
        R.string.example_extra_short,
        R.string.example_minor_group,
        R.string.example_major_group,
        R.string.example_syllable_break,
        R.string.example_linking
    ),

    val places_of_articulation: List<Int> = listOf(
        R.string.primary_stress,
        R.string.secondary_stress,
        R.string.long_vowel,
        R.string.half_long_vowel,
        R.string.extra_short_vowel,
        R.string.minor_foot_group,
        R.string.major_intonation_group,
        R.string.syllable_break,
        R.string.linking
    )
)

data class Tones(
    val sections: List<Int> = listOf(
        R.string.level,
        R.string.contour
    ),

    val levels: List<List<Int>> = listOf(
        listOf(R.string.example_level_extra_high, R.string.example_level_extra_high_2),
        listOf(R.string.example_level_high, R.string.example_level_high_2),
        listOf(R.string.example_level_mid, R.string.example_level_mid_2),
        listOf(R.string.example_level_low, R.string.example_level_low_2),
        listOf(R.string.example_level_extra_low, R.string.example_level_extra_low_2),
        listOf(R.string.example_level_downstep, R.string.empty_string),
        listOf(R.string.example_level_upstep, R.string.empty_string)
    ),

    val levels_places_of_articulation: List<Int> = listOf(
        R.string.extra_high,
        R.string.high,
        R.string.mid,
        R.string.low,
        R.string.extra_low,
        R.string.downstep,
        R.string.upstep
    ),

    val contours: List<List<Int>> = listOf(
        listOf(R.string.example_contour_rising, R.string.example_contour_rising_2),
        listOf(R.string.example_contour_falling, R.string.example_contour_falling_2),
        listOf(R.string.example_contour_high_rising, R.string.example_contour_high_rising_2),
        listOf(R.string.example_contour_low_rising, R.string.example_contour_low_rising_2),
        listOf(R.string.example_contour_rising_falling, R.string.example_contour_rising_falling_2),
        listOf(R.string.example_contour_rising, R.string.empty_string),
        listOf(R.string.example_contour_falling, R.string.empty_string)
    ),

    val contours_places_of_articulation: List<Int> = listOf(
        R.string.rising,
        R.string.falling,
        R.string.high_rising,
        R.string.low_rising,
        R.string.rising_falling,
        R.string.global_rise,
        R.string.global_fall
    ),

    val tones: List<List<List<Int>>> = listOf(
        levels,
        contours
    ),

    val tones_places_of_articulation: List<List<Int>> = listOf(
        levels_places_of_articulation,
        contours_places_of_articulation
    )
)

data class OtherSymbols(
    val sections: List<Int> = listOf(
        R.string.other_symbols
    ),

    val other_symbols: List<Int> = listOf(
        R.string.symbol_turned_w,
        R.string.symbol_lower_case_w,
        R.string.symbol_turned_h,
        R.string.symbol_small_capital_h,
        R.string.symbol_barred_reversed_glottal_stop,
        R.string.symbol_barred_glottal_stop,
        R.string.symbol_curly_tail_c,
        R.string.symbol_curly_tail_z,
        R.string.symbol_turned_long_leg_r,
        R.string.symbol_hooktop_heng
    ),

    val places_of_articulation: List<Int> = listOf(
        R.string.voiceless_labial_velar_fricative,
        R.string.voiced_labial_velar_approximant,
        R.string.voiced_labial_palatal_approximant,
        R.string.voiceless_epiglottal_fricative,
        R.string.voiced_epiglottal_fricative,
        R.string.epiglottal_plosive,
        R.string.voiceless_alveolo_palatal_fricative,
        R.string.voiced_alveolo_palatal_fricative,
        R.string.voiced_alveolar_lateral_flap,
        R.string.simulataneous_s_and_x
    )
)
