package com.vieira.pete.ipaapp


import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_ipa_symbol_info.*

class IpaSymbolInfoFragment : Fragment() {

    lateinit var typeface: Typeface

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        typeface = Typeface.createFromAsset(activity?.assets, "fonts/${this.resources.getString(R.string.font_subtitle)}")

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ipa_symbol_info, container, false)
    }

    fun setUpInfo(ipa: IpaSymbol) {
        symbolSymbol.text = resources.getString(ipa.symbol)
        symbolName.text = resources.getString(ipa.symbolName)
        symbolDescription.text = resources.getString(ipa.phoneticDescription)
        symbolExplanation.text = resources.getString(ipa.phoneticExplanation)

        headerName.typeface = typeface
        headerDescription.typeface = typeface
    }
}
