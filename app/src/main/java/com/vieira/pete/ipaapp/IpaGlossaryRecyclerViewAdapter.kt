package com.vieira.pete.ipaapp

import android.app.Activity
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.button_row.view.*
import kotlinx.android.synthetic.main.section_header.view.*
import kotlinx.android.synthetic.main.section_header_no_columns.view.*
import java.lang.ref.WeakReference

private const val FONT = "Exo2-BoldItalic.otf"

class IpaGlossaryRecyclerViewAdapter(sections: List<SectionData>,
                                    listener: Listener,
                                    private val sectionHeaderLayout: Int,
                                    private val numButtonsPerRow: Int)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), StickyHeaderInterface {

    private var middleText = ""
    private val tf = Typeface.createFromAsset((listener as Activity).assets, "fonts/$FONT")

    // Interface for sending click events to the activity
    interface Listener {
        fun onButtonClick(item: RowData, buttonIndex: Int)
    }

    var weakListener = WeakReference<Listener>(listener)

    // Inflates the item views (i.e. each row), based on the type of view
    override fun onCreateViewHolder(group: ViewGroup, type: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(group.context)
        val view = inflater.inflate(type, group, false)

        return when(type) {
            sectionHeaderLayout -> SectionViewHolder(view, sectionHeaderLayout)
            else -> RowViewHolder(view)
        }
    }

    // Grabs each item in the consonants list and applies it the a view
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, index: Int) {
        when {
            flattenedSections[index] is Int -> (holder as SectionViewHolder).bind(flattenedSections[index] as Int)
            flattenedSections[index] is RowData -> (holder as RowViewHolder).bind(flattenedSections[index] as RowData)
            else -> assert(false)
        }
    }

    override fun getItemCount() = flattenedSections.size

    override fun getItemViewType(position: Int): Int {
        return if (flattenedSections[position] is Int) {
            sectionHeaderLayout
        } else {
            R.layout.button_row
        }
    }

    fun setDisplayMiddleText(text: String) {
        this.middleText = text
    }

    // Holds the row view, with either title, titles, or title and buttons
    inner class RowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var layoutParams: ViewGroup.LayoutParams

        fun bind(item: RowData) {
            val itemViewButtons: List<Button> = listOf(itemView.button1, itemView.button2)

            itemView.rowTitle.typeface = tf

            if (itemView.layoutParams.height != 0) {
                layoutParams = itemView.layoutParams
            }
            // Bind row data text to row view subviews
            itemView.rowTitle.text = itemView.context.getString(item.rowTitle)

            for (i in 0 until item.buttons.size) {
                itemViewButtons[i].text = itemView.context.getString(R.string.empty_string)
                if (item.buttons[i] != R.string.empty_string) {
                    itemViewButtons[i].text = itemView.context.getString(item.buttons[i])
                }
            }

            if (numButtonsPerRow == 1) {
                itemViewButtons[1].visibility = View.GONE
            }

            // Bind button click listeners to listeners
            if (itemView.button1.text.isNotBlank()) {
                itemView.button1.setOnClickListener {
                    weakListener.get()?.onButtonClick(item, 0)
                }
                itemView.button1.isEnabled = true
            } else {
                itemView.button1.isEnabled = false
            }
            if (itemView.button2.text.isNotBlank()) {
                itemView.button2.setOnClickListener {
                    weakListener.get()?.onButtonClick(item, 1)
                }
                itemView.button2.isEnabled = true
            } else {
                itemView.button2.isEnabled = false
                itemView.middleText.visibility = View.INVISIBLE
            }

            // Hide or show row depending on if there's text in any of the buttons in the row
            itemView.visibility = View.GONE
            itemView.layoutParams = RecyclerView.LayoutParams(0, 0)
            for (button in item.buttons) {
                if (button != R.string.empty_string) {
                    itemView.visibility = View.VISIBLE
                    itemView.layoutParams = this.layoutParams
                    break
                }
            }

            // Show middleText in row if middleText contains text
            if (middleText.isNotEmpty()) {
                itemView.middleText.visibility = View.VISIBLE
                itemView.middleText.text = middleText
            }
        }
    }

    inner class SectionViewHolder(itemView: View,
                            private val sectionHeaderLayout: Int) : RecyclerView.ViewHolder(itemView) {

        fun bind(sectionTitle: Int) {
            if (sectionHeaderLayout == R.layout.section_header) {
                itemView.sectionTitle.text = itemView.context.getString(sectionTitle)
                itemView.column1Title.text = itemView.context.getString(R.string.unvoiced)
                itemView.column2Title.text = itemView.context.getString(R.string.voiced)
                itemView.sectionTitle.typeface = tf
                itemView.column1Title.typeface = tf
                itemView.column2Title.typeface = tf
            } else {
                itemView.sectionNoHeadersTitle.text = itemView.context.getString(sectionTitle)
                itemView.sectionNoHeadersTitle.typeface = tf
            }
        }
    }

    /* Flat (1D), ordered list of headers and rows from the section data passed into the class */
    val flattenedSections: List<Any> = sections.flatMap {
        listOf<Any>(it.sectionTitle) + it.rows }

    /*
     * StickyHeader Interface implementation
     */
    override fun getHeaderPositionForItem(itemPosition: Int): Int {
        var headerPosition: Int = 0
        var position: Int = itemPosition
        do {
            if (this.isHeader(position)) {
                headerPosition = position
                break
            }
            position -= 1
        } while (position >= 0)
        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int): Int = sectionHeaderLayout

    override fun bindHeaderData(headerView: View, headerPosition: Int) {
        if (sectionHeaderLayout == R.layout.section_header) {
            headerView.sectionTitle.text = headerView.context.getString(flattenedSections[headerPosition] as Int)
            headerView.column1Title.text = headerView.context.getString(R.string.unvoiced)
            headerView.column2Title.text = headerView.context.getString(R.string.voiced)
        } else {
            headerView.sectionNoHeadersTitle.text = headerView.context.getString(flattenedSections[headerPosition] as Int)
        }
    }

    override fun isHeader(itemPosition: Int): Boolean {
        return getItemViewType(itemPosition) == sectionHeaderLayout
    }
}
