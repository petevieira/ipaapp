package com.vieira.pete.ipaapp

import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Reference from
 * https://stackoverflow.com/questions/32949971/how-can-i-make-sticky-headers-in-recyclerview-without-external-lib
 */

class HeaderItemDecoration(recyclerView: RecyclerView, listener: StickyHeaderInterface) : RecyclerView.ItemDecoration() {
    // The object handling the callbacks from this class
    private var mListener: StickyHeaderInterface = listener
    // The height of the sticky header
    private var mStickyHeaderHeight: Int = 0

    override fun onDrawOver(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(canvas, parent, state)

        // Get top item in the RecyclerView that is visible to the user (at the top of the list)
        val topChild = parent.getChildAt(0) ?: return

        // Get the position of the top child in the RecyclerView. If the view is representing a
        // removed item it will return NO_POSITION
        val topChildPosition = parent.getChildAdapterPosition(topChild)
        if (topChildPosition == RecyclerView.NO_POSITION) return

        // Get the current header view using the top child position and
        // its parent (the RecyclerView)
        val overlayHeader = getHeaderViewForItem(topChildPosition, parent)

        // Set the layout size for the current header view
        fixLayoutSize(parent, overlayHeader)

        // Get the y position of the bottom of the current header
        val contactPoint: Int = overlayHeader.bottom

        // Get the child View that's in contact with the bottom of the current header View
//        val childInContact: View = getChildInContact(parent, contactPoint) ?: return
        val childInContact: View? = getChildInContact(parent, contactPoint)

        // If the child that's coming up into contact with the current header at the top is a
        // header, move the current header up as the user scrolls the new header up to replace
        // the current header
        var childIsAHeader: Boolean = false
        if (childInContact != null) {
            childIsAHeader = mListener.isHeader(parent.getChildAdapterPosition(childInContact))
        }

        // Move the header up as the user scrolls the next header to replace it
        if (childIsAHeader) {
            // Draw the moving header if the next header is pushing it up or if it is pushing
            // the next header down
            moveHeader(canvas, overlayHeader, childInContact!!)
        } else {
            // Draw the static header if the next header is not pushing it up
            drawHeader(canvas, overlayHeader)
        }
    }

    /**
     * Gets the header view for the current header, if it's the top-most child in the RecyclerView
     * @param position Position (index) of the header in the RecyclerView
     * @param parent The RecyclerView that we're working with
     * @return header View object after being filled in with the appropriate data
     */
    private fun getHeaderViewForItem(position: Int, parent: RecyclerView): View {
        // Index of the header from the RecyclerView.Adapter
        val headerPosition: Int = mListener.getHeaderPositionForItem(position)
        // The R.layout value for the header is the specified position
        val layoutResourceId: Int = mListener.getHeaderLayout(headerPosition)
        // The header View using the layout resource ID
        val header: View = LayoutInflater.from(parent.context).inflate(
            layoutResourceId, parent, false)
        // Set the data for the header view
        mListener.bindHeaderData(header, headerPosition)
        // Return the header view now that it's ready to be drawn for the user to see
        return header
    }

    private fun drawHeader(canvas: Canvas, overlayHeader: View) {
        canvas.save()
        canvas.translate(0.0f, 0.0f)
        overlayHeader.draw(canvas)
        canvas.restore()
    }

    /**
     * Move the current header up the amount that the next header has overlapped (pushed up) the
     * current header
     * @param canvas Canvas for the item decoration of the top header
     * @param overlayHeader View for the current header that hasn't gone out of view at the top yet
     * @param nextHeader View for the next header that will replace the current header, or get
     *                   pushed down by the current header if the user is swiping downwards
     */
    private fun moveHeader(canvas: Canvas, overlayHeader: View, nextHeader: View) {
        canvas.save()
        canvas.translate(0.0f, (nextHeader.top - overlayHeader.height).toFloat())
        overlayHeader.draw(canvas)
        canvas.restore()
    }

    /**
     * If a child view (row or header) is in contact with the
     */
    private fun getChildInContact(parent: RecyclerView, contactPoint: Int): View? {
        var childInContact: View? = null
        var child: View?
        for (i in 0..parent.childCount-1) {
            child = parent.getChildAt(i)
            if (child.bottom > contactPoint) {
                if (child.top <= contactPoint) {
                    // This child overlaps the contactPoint
                    childInContact = child
                    break
                }
            }
        }
        return childInContact
    }

    /**
     * Properly measures and lays out the top sticky header, where that header is the
     * view argument to the function.
     * @param parent The RecyclerView
     * @param view The top header view
     */
    private fun fixLayoutSize(parent: ViewGroup, view: View) {
        // Create specs for parent (RecyclerView), using the parent's exact width and
        // the parent's desired height, given the width
        val widthSpec: Int = View.MeasureSpec.makeMeasureSpec(
                parent.getWidth(), View.MeasureSpec.EXACTLY)
        val heightSpec: Int = View.MeasureSpec.makeMeasureSpec(
                parent.getHeight(), View.MeasureSpec.UNSPECIFIED)

        // Create specs for children (headers), using the parent's specs
        val childWidthSpec: Int = ViewGroup.getChildMeasureSpec(
                widthSpec,parent.paddingLeft + parent.paddingRight, view.layoutParams.width)
        val childHeightSpec: Int = ViewGroup.getChildMeasureSpec(
                heightSpec,parent.paddingTop + parent.paddingBottom, view.layoutParams.height)

        // Measure the stickyHeader's desired width and height based on its specs
        view.measure(childWidthSpec, childHeightSpec)
        // Save the measured height of the sticky header
        mStickyHeaderHeight = view.measuredHeight
        // Position the stickyHeader starting in the top left origin of the parent
        // and using the measured width and height of the parent
        view.layout(0, 0, view.measuredWidth, mStickyHeaderHeight)

    }
}

interface StickyHeaderInterface {
    /**
     * This method gets called by HeaderItemDecoration to fetch the position of the header
     * item in the adapter that is used for (represents) the item at the specified position.
     * @param itemPosition int. Adapter's position of the item for which to do the search of
     *                     the position of the item's header.
     * @return Int. Position of the header item in the adapter.
     */
    fun getHeaderPositionForItem(itemPosition: Int): Int

    /**
     * This method gets called by HeaderItemDecoration to get layout resource id for the
     * header item at specified adapter's position.
     * @param headerPosition int. Position of the header item in the adapter.
     * @return Int. Layout resource id.
     */
    fun getHeaderLayout(headerPosition: Int): Int

    /**
     * This method gets called by HeaderItemDecoration to set up the header View.
     * @param header View. Header to set the data on.
     * @param headerPosition Int. Position of the header item in the adapter.
     */
    fun bindHeaderData(headerView: View, headerPosition: Int)

    /**
     * This method gets called by HeaderItemDecoration to verify whether the
     * item represents a header
     * @param itemPosition Int
     * @return true, if item at the specified adapter's position represents a header
     */
    fun isHeader(itemPosition: Int): Boolean
}