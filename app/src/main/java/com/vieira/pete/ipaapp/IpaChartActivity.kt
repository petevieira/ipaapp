package com.vieira.pete.ipaapp

import android.gesture.Gesture
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import kotlinx.android.synthetic.main.activity_ipa_chart.*

private const val MAX_SCALE_FACTOR = 20.0f
private const val MIN_SCALE_FACTOR = 1.0f
private const val DOUBLE_TAP_SCALE_FACTOR = 3.0f

class IpaChartActivity : AppCompatActivity() {

    private var scaleFactor = 1.0f
    private var scaleGestureDetector: ScaleGestureDetector? = null
    private var gestureDetector: GestureDetector? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ipa_chart)

//        tableVowels.setStrokeWidth(1f)
//        tableVowels.setWillNotDraw(false)

//        scaleGestureDetector = ScaleGestureDetector(this, ScaleListener())
//        gestureDetector = GestureDetector(this, GestureListener())
        
//        consonantsNonPulmonicFragment.layoutParams.width = chartLayout.width/2
    }

//    override fun onTouchEvent(event: MotionEvent?): Boolean {
//        scaleGestureDetector?.onTouchEvent(event)
//        return true
//    }
//
//    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
//        gestureDetector?.onTouchEvent(ev)
//        scaleGestureDetector?.onTouchEvent(ev)
//        return true
//    }
//
//    private inner class ScaleListener: ScaleGestureDetector.SimpleOnScaleGestureListener() {
//        override fun onScale(detector: ScaleGestureDetector?): Boolean {
//            scaleFactor *= scaleGestureDetector!!.scaleFactor
//            scaleFactor = Math.max(MIN_SCALE_FACTOR, Math.min(scaleFactor, MAX_SCALE_FACTOR))
//            chartLayout.scaleX = scaleFactor
//            chartLayout.scaleY = scaleFactor
//            return true
//        }
//    }
//
//    private inner class GestureListener: GestureDetector.SimpleOnGestureListener() {
//        private var dx: Float = 0f
//        private var dy: Float = 0f
//        private var windowX: Float = 0f
//        private var windowY: Float = 0f
//        override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
//            dx -= distanceX
//            dy -= distanceY
//            windowX = (chartLayout.width*scaleFactor - chartLayout.width)/2
//            windowY = (chartLayout.height*scaleFactor - chartLayout.height)/2
//            dx = Math.max(Math.min(windowX, dx), -windowX)
//            dy = Math.max(Math.min(windowY, dy), -windowY)
//            chartLayout.animate()
//                    .x(dx)
//                    .y(dy)
//                    .setDuration(0)
//                    .start()
//
//            return true
//        }
//
//        override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
//            return super.onFling(e1, e2, velocityX, velocityY)
//        }
//
//        override fun onDoubleTap(e: MotionEvent?): Boolean {
//            if (scaleFactor >= DOUBLE_TAP_SCALE_FACTOR) {
//                scaleFactor = MIN_SCALE_FACTOR
//                chartLayout.animate()
//                        .scaleX(scaleFactor)
//                        .scaleY(scaleFactor)
//                        .x(0f)
//                        .y(0f)
//                        .setDuration(250)
//                        .start()
//            } else {
//                scaleFactor = DOUBLE_TAP_SCALE_FACTOR
//                dx = -(e!!.x - chartLayout.width/2)*scaleFactor
//                dy = -(e.y - chartLayout.height/2)*scaleFactor + chartLayout.height/2
//                chartLayout.animate()
//                        .scaleX(scaleFactor)
//                        .scaleY(scaleFactor)
//                        .x(dx)
//                        .y(dy)
//                        .setDuration(250)
//                        .start()
//            }
//
//            return true
//        }
//    }
}
