package com.vieira.pete.ipaapp

import android.content.Context
import android.graphics.Point
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.VideoView

// Time in milliseconds below which constitutes a tap, instead of a long touch or touch and drag
const val MAX_SINGLE_TOUCH_TIME_MS = 200
// Time in milliseconds before the end of the video after which a play will first rewind the video
// because playing the last bit of the video is pointless
const val REWIND_AFTER_PAUSED_MS_BEFORE_END = 50
var videoDragButtonDownTimestamp: Long = 0
var videoTouchButtonDownTimestamp: Long = 0

fun setUpVideoView(activity: Context?, video: VideoView, dragView: View, touchView: View, videoSuffix: Int, videoPreviewTimeMs: Int) {
    // If we don't have a video suffix, then there is no video for this symbol, so hide it
    if (activity?.resources?.getString(videoSuffix)!!.isEmpty()) {
        video.visibility = View.INVISIBLE
        return
    }

    // Load one of the videos in order to get it's dimensions
    val videoPath = "android.resource://${activity.packageName}/" + videoSuffix
    video.setVideoURI(Uri.parse(videoPath))

    // Adjust width of video to maintain correct aspect ratio
    video.setOnPreparedListener {
        val aspectRatio = (it.videoWidth.toFloat() / it.videoHeight.toFloat())
        video.layoutParams.width = (video.height * aspectRatio).toInt()
    }

    // What to do on touches
    dragView.setOnTouchListener { view: View, motionEvent: MotionEvent ->
        // Play and Pause the video
        if (motionEvent.action == MotionEvent.ACTION_UP) {
            if ((motionEvent.eventTime - videoDragButtonDownTimestamp) < MAX_SINGLE_TOUCH_TIME_MS) {
                if (video.isPlaying) {
                    video.pause()
                } else {
                    video.seekTo(0)
                    video.start()
                }
            }
        } else if (motionEvent.action == MotionEvent.ACTION_DOWN) {
            // Get Timestamp of touch
            videoDragButtonDownTimestamp = motionEvent.eventTime
        } else if (motionEvent.action == MotionEvent.ACTION_MOVE) {
            if ((motionEvent.eventTime - videoDragButtonDownTimestamp) > MAX_SINGLE_TOUCH_TIME_MS) {
                // Tracking video with touch and drag motion
                val newTimeMs = (motionEvent.x / view.width * video.duration).toInt()
                if (newTimeMs >= 0 && newTimeMs <= video.duration) {
                    video.seekTo(newTimeMs)
                }
            }
        }
        true
    }

    touchView.setOnTouchListener { _, motionEvent: MotionEvent ->
        // Play and Pause the video
        if (motionEvent.action == MotionEvent.ACTION_UP) {
            if ((motionEvent.eventTime - videoTouchButtonDownTimestamp) < MAX_SINGLE_TOUCH_TIME_MS) {
                if (video.isPlaying) {
                    video.pause()
                } else {
                    video.seekTo(0)
                    video.start()
                }
            }
        } else if (motionEvent.action == MotionEvent.ACTION_DOWN) {
            // Get Timestamp of touch
            videoTouchButtonDownTimestamp = motionEvent.eventTime
        }
        true
    }

    // What to do when the video completes
    video.setOnCompletionListener {
        video.seekTo(videoPreviewTimeMs)
    }
}

fun startVideoFromZero(context: Context?, video: VideoView, videoSuffix: Int) {
    if (context?.resources?.getString(videoSuffix)!!.isNotEmpty()) {
        // Video exists so rewind it and play it
        video.seekTo(0)
        video.start()
    }
}