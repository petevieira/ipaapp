package com.vieira.pete.ipaapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import kotlinx.android.synthetic.main.activity_ipa_generic.*
import kotlinx.android.synthetic.main.fragment_ipa_videos.*

class DiacriticsActivity : IpaGenericActivity() {

    init {
        val diacritics = Diacritics()
        sections = IntRange(0, diacritics.sections.size - 1).map {
            SectionData(diacritics.sections[it], IntRange(0, diacritics.diacritics.size - 1).map { row ->
                RowData(diacritics.places_of_articulation[row], listOf(diacritics.diacritics[row]))
            })
        }


        // Adapter for the RecyclerView for each row of header or buttons
        ipaAdapter = IpaGenericRecyclerViewAdapter(
                this.sections, this, R.layout.section_header_no_columns, 1)

        activityHasVideo = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_ipa_generic)
        setUpRecyclerView()
        setUpFragmentViewSizes()
        setUpTabCallbacks()

        supportFragmentManager.beginTransaction()
                .remove(supportFragmentManager.findFragmentById(R.id.ipaVideosFragment) as IpaVideosFragment)
                .commit()

        // Set up video and symbol info fragments with first symbol in activity
        // so that they're not just awkwardly blank.
        setUpIpaViews(symbolsMap.get(R.string.example_voiceless))
    }
}