package com.vieira.pete.ipaapp

import android.app.Activity
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.button_row.view.*
import kotlinx.android.synthetic.main.section_header.view.*
import kotlinx.android.synthetic.main.section_header_no_columns.view.*
import java.lang.ref.WeakReference

class IpaGenericRecyclerViewAdapter(sections: List<SectionData>,
                                    listener: Listener,
                                    private val sectionHeaderLayout: Int,
                                    private val numButtonsPerRow: Int)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), StickyHeaderInterface {

    private var middleText = R.string.empty_string
    lateinit var typeface: Typeface

    // Interface for sending click events to the activity
    interface Listener {
        fun onButtonClick(item: RowData, buttonIndex: Int)
    }

    var weakListener = WeakReference<Listener>(listener)

    // Inflates the item views (i.e. each row), based on the type of view
    override fun onCreateViewHolder(group: ViewGroup, type: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(group.context)
        val view = inflater.inflate(type, group, false)

        return when(type) {
            sectionHeaderLayout -> SectionViewHolder(view, sectionHeaderLayout)
            else -> RowViewHolder(view)
        }
    }

    // Grabs each item in the consonants list and applies it the a view
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, index: Int) {
        when {
            flattenedSections[index] is Int -> (holder as SectionViewHolder).bind(flattenedSections[index] as Int)
            flattenedSections[index] is RowData -> (holder as RowViewHolder).bind(flattenedSections[index] as RowData)
            else -> assert(false)
        }
    }

    override fun getItemCount() = flattenedSections.size

    override fun getItemViewType(position: Int): Int {
        return if (flattenedSections[position] is Int) {
            sectionHeaderLayout
        } else {
            R.layout.button_row
        }
    }

    /**
     * Sets the string for the text between button one and two
     */
    fun setDisplayMiddleText(text: Int) {
        this.middleText = text
    }

    /**
     * Sets the typeface for the titles
     */
    fun setTitlesTypeface(typeface: Typeface) {
        this.typeface = typeface
    }

    /**
     * Holds the row view, with either title, titles, or title and buttons
     */
    inner class RowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var layoutParams: ViewGroup.LayoutParams

        fun bind(item: RowData) {
            val itemViewButtons: List<Button> = listOf(itemView.button1, itemView.button2)

            // Set the typeface of the row title
            itemView.rowTitle.typeface = typeface

            if (itemView.layoutParams.height != 0) {
                layoutParams = itemView.layoutParams
            }

            // Set row title TextView text to row title in item (Binding data to view)
            itemView.rowTitle.text = itemView.context.getString(item.rowTitle)
            itemView.middleText.visibility = View.VISIBLE

            // For each button clear the text since these views get recycled, and
            // then if the data for that button contains text, then set it.
            for (i in 0 until item.buttons.size) {
                itemViewButtons[i].text = itemView.context.getString(R.string.empty_string)
                itemView.middleText.text = itemView.context.getString(middleText)
                // button i has text...
                if (item.buttons[i] != R.string.empty_string) {
                    // Set button text
                    itemViewButtons[i].text = itemView.context.getString(item.buttons[i])
                    // Set button click callback function
                    itemViewButtons[i].setOnClickListener { weakListener.get()?.onButtonClick(item, i) }
                    // Enable button
                    itemViewButtons[i].isEnabled = true
                    // If button two has text...
                    if (i == 1) {
                        // Make the middle text visible
                        itemView.middleText.visibility = View.VISIBLE
                    }
                } else {
                    // Clear click callback function
                    itemViewButtons[i].setOnClickListener {  }
                    // Disable button
                    itemViewButtons[i].isEnabled = false
                    if (i == 1) {
                        itemView.middleText.visibility = View.INVISIBLE
                    }
                }
            }

            // If there's only one column of buttons for this activity (set in the activity)...
            if (numButtonsPerRow == 1) {
                // then get rid of the second button
                itemViewButtons[1].visibility = View.GONE
                itemView.middleText.visibility = View.GONE
            }

            // Hide or show row depending on if there's text in any of the buttons in the row
            itemView.visibility = View.GONE
            itemView.layoutParams = RecyclerView.LayoutParams(0, 0)
            for (button in item.buttons) {
                if (button != R.string.empty_string) {
                    itemView.visibility = View.VISIBLE
                    itemView.layoutParams = this.layoutParams
                    break
                }
            }
        }
    }

    inner class SectionViewHolder(itemView: View,
                            private val sectionHeaderLayout: Int) : RecyclerView.ViewHolder(itemView) {

        fun bind(sectionTitle: Int) {
            if (sectionHeaderLayout == R.layout.section_header) {
                itemView.sectionTitle.text = itemView.context.getString(sectionTitle)
                itemView.column1Title.text = itemView.context.getString(R.string.unvoiced)
                itemView.column2Title.text = itemView.context.getString(R.string.voiced)
                itemView.sectionTitle.typeface = typeface
                itemView.column1Title.typeface = typeface
                itemView.column2Title.typeface = typeface
                // Try setting margins, etc here
            } else {
                itemView.sectionNoHeadersTitle.text = itemView.context.getString(sectionTitle)
                itemView.sectionNoHeadersTitle.typeface = typeface
            }
        }
    }

    /* Flat (1D), ordered list of headers and rows from the section data passed into the class */
    val flattenedSections: List<Any> = sections.flatMap {
        listOf<Any>(it.sectionTitle) + it.rows }

    /*
     * StickyHeader Interface implementation
     */
    override fun getHeaderPositionForItem(itemPosition: Int): Int {
        var headerPosition: Int = 0
        var position: Int = itemPosition
        do {
            if (this.isHeader(position)) {
                headerPosition = position
                break
            }
            position -= 1
        } while (position >= 0)
        return headerPosition
    }

    override fun getHeaderLayout(headerPosition: Int): Int = sectionHeaderLayout

    override fun bindHeaderData(headerView: View, headerPosition: Int) {
        if (sectionHeaderLayout == R.layout.section_header) {
            headerView.sectionTitle.text = headerView.context.getString(flattenedSections[headerPosition] as Int)
            headerView.column1Title.text = headerView.context.getString(R.string.unvoiced)
            headerView.column2Title.text = headerView.context.getString(R.string.voiced)
            headerView.sectionTitle.typeface = typeface
            headerView.column1Title.typeface = typeface
            headerView.column2Title.typeface = typeface
        } else {
            headerView.sectionNoHeadersTitle.text = headerView.context.getString(flattenedSections[headerPosition] as Int)
            headerView.sectionNoHeadersTitle.typeface = typeface
        }
    }

    override fun isHeader(itemPosition: Int): Boolean {
        return getItemViewType(itemPosition) == sectionHeaderLayout
    }
}
