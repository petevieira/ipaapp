package com.vieira.pete.ipaapp

import android.animation.ValueAnimator
import android.graphics.Point
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.method.Touch.scrollTo
import android.util.Log
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_ipa_generic.*
import kotlinx.android.synthetic.main.fragment_ipa_symbol_info.*
import kotlinx.android.synthetic.main.fragment_ipa_videos.*


open class IpaGlossaryActivity : AppCompatActivity(), IpaGlossaryRecyclerViewAdapter.Listener {

    private var lastButtonTouched: IpaSymbol? = null
    private var currentTab: IpaGenericActivity.TabEnum = IpaGenericActivity.TabEnum.TAB_ANIMATION

    // Storage of all the ipa sections for the specific activity
    open lateinit var sections: List<SectionData>

    // Adapter for the RecyclerView for each row of header or buttons
    open lateinit var ipaAdapter: IpaGenericRecyclerViewAdapter

    protected fun setUpRecyclerView() {
        // Add layout manager to consonants recycler view
        recyclerViewIpaGeneric.layoutManager = LinearLayoutManager(this)

        // Add adapter and pass in filled-in sections data
        recyclerViewIpaGeneric.adapter = ipaAdapter

        // Enable sticky headers
        recyclerViewIpaGeneric.addItemDecoration(
                HeaderItemDecoration(recyclerViewIpaGeneric,
                        ipaAdapter as StickyHeaderInterface))
    }

    open fun setUpFragmentViewSizes() {
        // Set the column sizes for the symbol information text below the video
        val displaySize = Point(ipaGenericLayout.layoutParams.width,
                ipaGenericLayout.layoutParams.height)
        windowManager.defaultDisplay.getSize(displaySize)
        symbolSymbol.layoutParams.width = displaySize.x * 1 / 9
        symbolName.layoutParams.width = displaySize.x * 3 / 9
        symbolDescription.layoutParams.width = displaySize.x * 5 / 9
        headerName.layoutParams.width = displaySize.x * 3 / 9
        headerDescription.layoutParams.width = displaySize.x * 5 / 9
    }

    protected fun setUpTabCallbacks() {
        //        tabAnimation.setOnClickListener {
//            currentTab = Tab.TAB_ANIMATION
//            val haveAnimationVideo = resources.getString(lastButtonTouched!!.videoAnimatedSuffix).isNotEmpty()
//            videoConsonantsPulmonicAnimated.visibility = View.VISIBLE
//            videoConsonantsPulmonic.visibility = View.INVISIBLE
//            tabAnimation.setBackgroundColor(R.attr.fillColorButtonPressed)
//            tabMri.setBackgroundColor(R.attr.fillColorButton)
//            var typedValue = TypedValue()
//            theme.resolveAttribute(R.attr.fillColorButtonPressed, typedValue, true);
//            tabAnimation.setBackgroundColor(typedValue.data)
//            theme.resolveAttribute(R.attr.fillColorButton, typedValue, true);
//            tabMri.setBackgroundColor(typedValue.data)
//            if (haveAnimationVideo) {
//                setUpVideoView(this,
//                        videoConsonantsPulmonicAnimated,
//                        videoConsonantsPulmonicTouchDragView,
//                        videoConsonantsPulmonicSingleTouchView,
//                        lastButtonTouched!!.videoAnimatedSuffix,
//                        lastButtonTouched!!.videoAnimatedPreviewTimeMs)
//            }
//            startVideoFromZero(videoConsonantsPulmonicAnimated, lastButtonTouched!!.videoAnimatedSuffix)
//        }
//
//        tabMri.setOnClickListener {
//            currentTab = Tab.TAB_MRI
//            // Do we have an MRI video?
//            val haveMriVideo = resources.getString(lastButtonTouched!!.videoMriSuffix).isNotEmpty()
//            // Make MRI video visible and animated one invisible
//            videoConsonantsPulmonic.visibility = View.VISIBLE
//            videoConsonantsPulmonicAnimated.visibility = View.INVISIBLE
//            // Why am I doing this?
//            val typedValue = TypedValue()
//            theme.resolveAttribute(R.attr.fillColorButtonPressed, typedValue, true);
//            tabMri.setBackgroundColor(typedValue.data)
//            theme.resolveAttribute(R.attr.fillColorButton, typedValue, true);
//            tabAnimation.setBackgroundColor(typedValue.data)
//
//            // If MRI video exists, set it up
//            if (haveMriVideo) {
//                setUpVideoView(this,
//                        videoConsonantsPulmonic,
//                        videoConsonantsPulmonicTouchDragView,
//                        videoConsonantsPulmonicSingleTouchView,
//                        lastButtonTouched!!.videoMriSuffix,
//                        lastButtonTouched!!.videoMriPreviewTimeMs)
//            }
//            // Play the MRI video from the beginning
//            startVideoFromZero(videoConsonantsPulmonic, lastButtonTouched!!.videoMriSuffix)
//        }
    }

    private fun setUpTabs(ipa: IpaSymbol) {
        val haveAnimationVideo = resources.getString(ipa.videoAnimatedSuffix).isNotEmpty()
        val haveMriVideo = resources.getString(ipa.videoMriSuffix).isNotEmpty()

        // Button has changed. If have video, show tab. If not, hide tab and uncheck it
        if (lastButtonTouched != ipa) {
//            tabAnimation.visibility = if (haveAnimationVideo) View.VISIBLE else View.INVISIBLE
//            tabMri.visibility = if (haveMriVideo) View.VISIBLE else View.INVISIBLE
        }
    }

    /*-----------*/
    /* Callbacks */
    /*-----------*/
    override fun onButtonClick(item: RowData, buttonIndex: Int) {
        val ipa: IpaSymbol? = symbolsMap.get(item.buttons[buttonIndex])
        if (ipa == null) {
            Log.d("Got null symbol:", resources.getString(item.buttons[buttonIndex]))
            return
        }
        setUpIpaViews(ipa)
    }

    protected fun setUpIpaViews(ipa: IpaSymbol?) {
        if (ipa != null) {
            setUpTabs(ipa)
            (supportFragmentManager.findFragmentById(R.id.ipaSymbolInfoFragment) as IpaSymbolInfoFragment).setUpInfo(ipa)
            if (lastButtonTouched == null) {
                (supportFragmentManager.findFragmentById(R.id.ipaVideosFragment) as IpaVideosFragment).setUpVideo(ipa, lastButtonTouched, currentTab, false)
                this.indicateScrollingAffordance()
            } else {
                (supportFragmentManager.findFragmentById(R.id.ipaVideosFragment) as IpaVideosFragment).setUpVideo(ipa, lastButtonTouched, currentTab)
            }
            lastButtonTouched = ipa
        }
    }

    private fun indicateScrollingAffordance() {
        scrollLayout.scrollTo(0, 100)
        scrollLayout.animate()
                .alpha(1f)
                .setDuration(300)
                .start()

        val ani: ValueAnimator = ValueAnimator.ofInt(100, 0)
        ani.addUpdateListener {
            val value = it.animatedValue as Int
            scrollLayout.scrollTo(0, value)
        }
        ani.interpolator = LinearInterpolator()
        ani.duration = 750
        ani.start()
    }
}

/*
 Resulting action     | Click new button with animation | Click new button with only MRI | Click new button w/no videos | Click animation tab | Click MRI tab
 -------------------------------------------------------------------------------------------------------------------------------------------------------------
 Play animation video | Yes                             | No                             | No                           | Yes                 | No

 Play MRI video       | No                              | Yes                            | No                           | No                  | Yes

 Do nothing           | No                              | No                             | Yes                          | No                  | No

 Show animation tab   | Yes                             | No                             | No                           | No                  | No

 Hide animation tab   | No                              | Yes                            | Yes                          | No                  | Yes

 Show MRI tab         | No                              | Yes                            | No                           | No                  | No

 Hide MRI tab         | Yes                             | No                             | Yes                          | No                  | No
 */