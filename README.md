## Status
- I have abandoned this project due to licensing issues.

## [Demo video](https://www.youtube.com/watch?v=qYPipMc9xDY)

To Do
----

1. Add info about each symbol when pressed
    - Unicode
    - Example words is several languages
    - Link to chart showing which languages have that sound
    
2. Add rest of main activities (DONE)
    - Other Symbols (DONE)
    - Diacritics (DONE)
    - Tones (DONE)
    - IPA Chart (DONE)

4. Add other non-ipa symbols (like underdotting for retroflex. ref: p31 Phonology In Generative Grammar Kenstowicz)

3. Add all the correct videos

4. Add back button for phones without one

5. Update main activity design (grid view with pictures?)

6. Add spectrogram below videos

7. Update color theme and design

8. Charts for languages

9. Stationary highlighting of places of articulation on top of videos not during playback

10. Recolor videos so they are seamless with color theme

Videos
----
1. There was a problem with the video tracking because the videos didn't have enough keyframes,
so you can use this command with ffmpeg to add keyframes every 2 frames of the video
	- ffmpeg -i "~/Downloads/play_video_test.mp4" -c:v libx264 -preset superfast -x264opts keyint=2 -acodec copy -f mp4 "~/Downloads/play_video_test_key_frame.mp4"
	- info found at https://www.dev2qa.com/how-to-make-android-videoview-seekto-method-consistent/

1. Changing the dimensions (cropping the video)
	- Crop video to origin=(0,input_height/2-100), size=(input_width x 400)
		- ffmpeg -i "./Sequence #1.mp4" -filter:v "crop=iw:400:0:ih/2-100" Sequence\ #1-testcrop.mp4

1. Changing a certain colored pixel to a different colored pixel
	- If r value of pixel is 0, change it to 3; if green value of pixel is 0, change it to 3; if blue value of pixel is 0, change it 3.
	- ffmpeg -i "./alveolar_fricative_voiced_animated" -filter:v "lutrgb=r='if(eq(val,0),3,val)':g='if(eq(val,0),3,val)':b='if(eq(val,0),3,val)'"

	1. Swap whites and blacks
	2. Convert blacks to dark purple
	3. Convert whites to light purple
	ffmpeg -i "./alveolar_fricative_voiced_animated.mp4" -filter:v "lutrgb=r=negval:g=negval:b=negval, lutrgb=r='if(lt(val,128),124,val)':g='if(lt(val,128),0,val)':b='if(lt(val,128),94,val)', lutrgb=r='if(gt(val,124),204,val)':g='if(gt(val,128),164,val)':b='if(gt(val,128),186,val)', crop=iw:450:0:ih/2-50" -c:v libx264 -preset superfast -x264opts keyint=2 -acodec copy -f mp4 "output.mp4"

1. Changing MRI videos from SeeingSpeech

- Animated Videos
ffmpeg -i alveolar_fricative_voiced_animated.mp4 -filter:v "lutrgb=r=negval:g=negval:b=negval, lutrgb=r='if(gt(val,230),255,val)':g='if(gt(val,230),255,val)':b='if(eq(val,230),255,val)', lutrgb=r='(val*102/255)+102':g='val*164/255':b='(val*113/255)+73', crop=iw:450:0:ih/2-50" -pix_fmt yuv420p -c:v libx264 -preset superfast -x264opts keyint=2 -acodec copy -f mp4 output.mp4

- MRI Videos
ffmpeg -i alveolar_lateral_click.mp4 -filter:v "lutrgb=r=negval:g=negval:b=negval, lutrgb=r='(val*102/255)+102':g='val*164/255':b='(val*113/255)+73', crop=64:48:24:14" -pix_fmt yuv420p -c:v libx264 -preset superfast -x264opts keyint=2 -acodec copy -f mp4 output.mp4

ffmpeg -i alveolar_fricative_voiced_animated.mp4 -filter:v "lutrgb=r=negval:g=negval:b=negval, crop=iw-24:ih-20:24:0" -pix_fmt yuv420p -c:v libx264 -preset superfast -x264opts keyint=2 -acodec copy -f mp4 output.mp4


References
----
1. International Phonetic Association. 1949 (repr. 1967). The Principles of the International Phonetic Assocation. London: Dept. of Phonetics, University College (now Dept. of Phonetics and Linguistics, University College London).
1. Lawson, E., J. Stuart-Smith, J. M. Scobbie, S. Nakai (2018). Seeing Speech: an articulatory web resource for the study of Phonetics. University of Glasgow. 5th December 2019. https://seeingspeech.ac.uk
1. Phonetic Symbol Guide. Geoffrey K. Pullum and William A. Ladusaw. 2nd Edition. The University of Chicago Press. 1996.
1. Handbook of the International Phonetic Assocation: A Guide to the Use of the International Phonetic Alphabet. Cambridge University Press. 1999.
1. Wikipedia


