#!/bin/bash

# Get working directory of this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Set directory where videos are
VIDEO_DIR="$DIR/../app/src/main/res/raw"
TOP_DIR="$DIR/.."

# Get list of animated video files including full paths and file extensions
ANIMATED_FILES="$(find $VIDEO_DIR -name "*_animated.mp4")"

# Create directory to store converted files temporarily
CONVERTED_AUDIO_DIR="${TOP_DIR}/converted_audios"
mkdir -p ${CONVERTED_AUDIO_DIR}
rm ${CONVERTED_AUDIO_DIR}/*.wav

for filepath in $ANIMATED_FILES; do
	# Get name of file without path
	file=$(basename "$filepath")

	# Convert video to mono audio file
	ffmpeg -i "$filepath" -ac 1 "${CONVERTED_AUDIO_DIR}/${file}_audio.wav"
done
