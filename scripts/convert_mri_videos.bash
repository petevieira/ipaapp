#!/bin/bash
SUFFIX="mri"

# Get working directory of this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Set directory where videos are
VIDEO_DIR="$DIR/../app/src/main/res/raw"
TOP_DIR="$DIR/.."

# Copy video originals to raw directory
VIDEOS_BACKUP="${TOP_DIR}/videos_backup"
cp ${VIDEOS_BACKUP}/*_${SUFFIX}.mp4 ${VIDEO_DIR}/

# Get list of video files including full paths and file extensions
VIDEO_FILES="$(find $VIDEO_DIR -name "*_${SUFFIX}.mp4")"

# Create directory to store converted files temporarily
CONVERTED_VIDEO_DIR="${TOP_DIR}/converted_videos"
mkdir -p ${CONVERTED_VIDEO_DIR}
rm ${CONVERTED_VIDEO_DIR}/*_${SUFFIX}.mp4

for filepath in $VIDEO_FILES; do
	# Get name of file without path
	file=$(basename "$filepath")

	# Invert video background from black to white, and invert video head from white to black,
	# convert black to dark purple
	# convert white to light purple
	# crop video to top/left origin=(0,input_height/2-50), size=(input_width x 450)
	# And add keyframes
	ffmpeg -i "$filepath" -filter:v \
	"lutrgb=r=negval:g=negval:b=negval, lutrgb=r='(val*102/255)+102':g='val*164/255':b='(val*113/255)+73', crop=64:48:24:14" \
	-pix_fmt yuv420p -c:v libx264 -preset superfast -x264opts keyint=2 -acodec copy -f mp4 "${CONVERTED_VIDEO_DIR}/$file"

	rm $filepath
done

# Copy newly converted videos into raw directory
mv ${CONVERTED_VIDEO_DIR}/*_${SUFFIX}.mp4 ${VIDEO_DIR}/